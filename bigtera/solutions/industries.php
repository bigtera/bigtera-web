<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - Industries";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">Industries</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">Home</a></li>
				<li><a href="">Solutions</a></li>
				<li>Industries</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-weight-700 pi-text-base">Streaming Media</p>
		<p class="lead-14">
			Companies that host streaming media services find themselves in a unique situation. They must host large amounts of data while delivering an acceptable level of performance for customers that stream their content. These companies are constantly expanding their storage to keep up with content that is ever increasing. Bigtera products.
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>High performance (throughput): Data delivery is a critical aspect of hosting content that will be streamed. All Bigtera products support a scale out architecture that improves in throughput performance as the infrastructure scales out.</li>
						<li>Scale out architecture: The amount of content a streaming media company must host is ever increasing. The scale out architecture of all Bigtera products offer streaming media companies a cost effective pay-as-you-grow model.</li>
						<li>Define QoS: Customers demand a certain level of service when they are streaming content from a host companies library. All Bigtera products support administrator defined quaility of service (IOPS and throughput) for each Virtual Storage area where content is stored.</li>
					</ul>
				</div>
			</div>
		</p>
		<p class="lead-26 pi-weight-700 pi-text-base">Video Surveillance</p>
		<p class="lead-14">
			Companies that host video surveillance companies seem to face issues similar to that of video streaming service companies. An ever expanding amount of data is gathered and throughput is critical for data delivery while the NVR content is being stored.
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>Scale out architecture: The amount of content a video serveillance company must store is ever increasing. The scale out architecture of all Bigtera products offer a cost effective pay-as-you-grow model.</li>
						<li>High performance (throughput): Data delivery is a critical aspect of saving video serveillance content (NVR files). All Bigtera products support a scale out architecture that improves in throughput performance as the infrastructure scales out. </li>
						<li>Define QoS: A certain level of service is required to save video serveillance content (NVR files). All Bigtera products support administrator defined quaility of service (IOPS and throughput) for each Virtual Storage area where content is stored.</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>