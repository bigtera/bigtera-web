<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - Virtualization";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">Virtualization</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">Home</a></li>
				<li><a href="">Solutions</a></li>
				<li>Virtualization</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-weight-700 pi-text-base">Make scalability, agility, and efficiency part of your virtual infrastructure</p>
		<p class="lead-14">
			Most businesses have moved a portion of their data center infrastructure from physical machines to the virtual machines. This move occurred because CIOs wanted to increase the return on their current infrasturcture investment. This was accomplished by consolidating their physical servers into virtual machines. This allowed several virtual machines to run on a single physical server. This meant that compute and storage resources were utilized in a much more efficient way.
		</p>
		<p class="lead-14">
			With the advent of private clouds running big data applications and solutions, there has been an explosion of data and application requirements. CIOs are now looking for ways to make the expansion of their data centers cost effective and even more efficient. Hyper-converged storage solutions and software-defined storage provide the solutions to these issues.
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>Consolidate and converge: Consolidating storage resources and converging compute and storage resources into a single resource pool affords a great deal of agility when assigning resources. This is potent when Bigtera’s products support the scaling out and up of storage and compute resources.</li>
						<li>Improve customer satisfaction: With compute and storage resources consolidated and converged, administrators can specify the QoS (IOPS and throughput) of their storage. This gives a great deal of flexibility when it comes to how mission critical applications and data are serviced. When combined with data tiering, mission critical applications and data are given the highest priority. Better service delivered to those applications and data mean increased customer satisfaction.</li>
						<li>Efficient storage: A number of data services contribute to making the most of your storage resources. The first is thin provisioning. Thin provisioning provides the virtual illusion of physical resources to applications and users. Resources are then allocated just-in-time for applications when they are needed. Data compression and deduplication offer the next level of efficiency. Compression increases the density of stored data while deduplication eliminates redundant data blocks.</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>