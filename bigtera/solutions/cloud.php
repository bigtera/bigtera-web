<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - Cloud";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(../img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">Cloud</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">Home</a></li>
				<li><a href="">Solutions</a></li>
				<li>Cloud</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-weight-700 pi-text-base">Take your public and hybrid clouds to the next level</p>
		<p class="lead-14">
			Cost efficiency is one of the major reasons businesses have turned to cloud storage and services. This is due to the fact that the cloud provides the most cost effective resources available for storage and computing power. Bigtera can help bring even greater cost efficiency and protection to your clouds and improve the performance between your private and public clouds.
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>Increased ROI : Make your cloud storage go further with data compression and deduplication. Compression and deduplication can significantly reduce the amount of space that data requires. Data compression does this by storing the data in a much denser format. Deduplication eliminates duplicated blocks of data.</li>
						<li>Ensure data security: Data protection is a critical component to ensure the business continuity of your infrstructure. Erasure coding, data encryption, and remote backups to the cloud keep your data safe and your business running. Bigtera products can encrypt data using Intel’s AES-NI. Bigtera products can also use erasure coding to protect your data. Erasure coding breaks data into smaller blocks, duplicates the blocks, stores the blocks across your storage infrastructure, and then makes a parity file and stores that file elsewhere in your infrastructure. Finally, Bigtera products can perform remote backups from your private cloud, or your onpremise storage infrasturcture, to cloud storage (Amazon S3 or OpenStack SWIFT).</li>
						<li>Performance: Data delivery can have a huge impact on the effectiveness of interactions with your clouds. Bigtera gives you the ability to control the throughput of data to and from your cloud.</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>
