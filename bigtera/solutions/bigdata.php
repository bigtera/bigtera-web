<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - Big Data";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">Big Data</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">Home</a></li>
				<li><a href="">Solutions</a></li>
				<li>Big Data</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled" style="padding-bottom:100px">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-weight-700 pi-text-base">Bigtera brings out the best of Big Data</p>
		<p class="lead-14">
			Buisnesses are interested in big data for a variety of reasons. The primary reasons lie in the insights and sound business descisions that can be made only by analysis of vast amounts of data. Bigtera can help solve some of the largest issues that companies encounter when dealing with big data.
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>High performance (IOPS): Cloud computing applications require a great deal of processing power to analyse and visualize data. Bigtera supports flash-based SSD acceleration to bring the computing power needed for big data.</li>
						<li>High performance (throughput): Data delivery is critical for big data applications and storage. All Bigtera products scale up and out and add to throughput performance for best data delivery performance.</li>
						<li>Cost effective capacity: Bigtera products scale up and out which provides cost effective scalability ideally suited for big data applications and storage. Adding resources to your infrastructure results in no disruption of service when Bigtera products are deployed in the data center. This allows administrators to add resources quickly when they need them.</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>