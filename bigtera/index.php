<?php include_once('config.php'); ?>
<?php
$metaD = "The world's first user-definable,storage-revitalizing platform.Bigtera VirtualStor™ is the world's first software-defined storage (SDS) platform that virtually aggregates, consolidates, and optimizes all storage resources (SAN and NAS) and provides administrators unrestricted flexibility in defining how those resources are assigned and utilized.";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w" style="background-image: url(<?php echo $url; ?>img_external/revolution-slider/landbg.jpg); background-size: cover; background-position: 50% 50%; color:#fff">

<!-- Row -->
<div class="pi-row pi-grid-small-margins">
<div class="pi-section pi-padding-top-20 pi-padding-bottom-30" style="overflow:hidden">
  <!-- Col 2 -->
  <div class="pi-col-sm-8 landtagline">
    <h3 class="lead-34 pi-letter-spacing pi-weight-400 pi-margin-bottom-20">The world's first user-definable,<br>storage-revitalizing platform.</h3>
    <p class="lead-14 pi-margin-bottom-30">Bigtera VirtualStor™ is the world's first software-defined storage (SDS) platform that virtually aggregates, consolidates, and optimizes all storage resources (SAN and NAS) and provides administrators unrestricted flexibility in defining how those resources are assigned and utilized.</p>
  </div>
  <!-- End col 2 -->
  <!-- Col 2 -->
  <div class="pi-col-sm-4 pi-center pi-padding-top-10 animated flipInY delay">
    <img src="<?php echo $url; ?>img_external/revolution-slider/landimg.png" alt="">
  </div>
  <!-- End col 2 -->
</div>
</div>
<!-- End Row -->

</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->
	
<div class="pi-section-w pi-section-white">
	<div class="pi-section pi-padding-bottom-20">
		
		<!-- Row -->
		<div class="pi-row pi-grid-big-margins">
			
			<!-- Col 3 -->
			<div class="pi-col-xs-4 pi-padding-bottom-40">
			
				<div class="pi-icon-box-vertical pi-icon-box-vertical-icon-bigger pi-text-center animated" data-animation="bounce" data-animation-delay="1000">
				
					<div class="pi-icon-box-icon pi-icon-box-icon-circle pi-icon-box-icon-base" style="background: #eef1f1;">
						<img src="<?php echo $url; ?>img_external/icons/80px/product_1.png" alt="">
					</div>
					
					<h5 class="pi-weight-700 pi-uppercase pi-letter-spacing"><a href="<?php echo $url; ?>product.php#sds" class="pi-link-dark">Software-Defined Storage</a></h5>

					<p class="pi-margin-bottom-10">
						Are you looking to transition your existing infrastructure in to a software-defined data center. 
					</p>
					
					<p>
						<a href="<?php echo $url; ?>product.php#sds">Learn more<i class="icon-right-open-mini pi-icon-right"></i></a>
					</p>
					
				</div>
				
			</div>
			<!-- End col 3 -->
			
			<!-- Col 3-->
			<div class="pi-col-xs-4 pi-padding-bottom-40">
			
				<div class="pi-icon-box-vertical pi-icon-box-vertical-icon-bigger pi-text-center animated" data-animation="bounce" data-animation-delay="1000">
				
					<div class="pi-icon-box-icon pi-icon-box-icon-circle pi-icon-box-icon-base" style="background: #eef1f1;">
						<img src="<?php echo $url; ?>img_external/icons/80px/product_2.png" alt="">
					</div>

					<h5 class="pi-weight-700 pi-uppercase pi-letter-spacing"><a href="<?php echo $url; ?>product.php#converger" class="pi-link-dark">Hyper-Converged Storage</a></h5>

					<p class="pi-margin-bottom-10">
						Are you looking for a cost effective solution for your virtual servers and VDI.
					</p>
					
					<p>
						<a href="<?php echo $url; ?>product.php#converger">Learn more<i class="icon-right-open-mini pi-icon-right"></i></a>
					</p>
					
				</div>
				
			</div>
			<!-- End col 3 -->
			
			<!-- Col 3 -->
			<div class="pi-col-xs-4 pi-padding-bottom-40">
			
				<div class="pi-icon-box-vertical pi-icon-box-vertical-icon-bigger pi-text-center animated" data-animation="bounce" data-animation-delay="1000">
				
					<div class="pi-icon-box-icon pi-icon-box-icon-circle pi-icon-box-icon-base" style="background: #eef1f1;">
						<img src="<?php echo $url; ?>img_external/icons/80px/product_3.png" alt="">
					</div>

					<h5 class="pi-weight-700 pi-uppercase pi-letter-spacing"><a href="<?php echo $url; ?>product.php#scaler" class="pi-link-dark">Scale Out Storage</a></h5>

					<p class="pi-margin-bottom-10">
						Are you looking to scale performance with storage capacity.
					</p>
					
					<p>
						<a href="<?php echo $url; ?>product.php#scaler">Learn more<i class="icon-right-open-mini pi-icon-right"></i></a>
					</p>
					
				</div>
				
			</div>
			<!-- End col 3 -->
			
		</div>
		<!-- End row -->
				
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>