<!-- Menu -->
<div class="pi-header-block pi-pull-right">
<ul class="pi-simple-menu pi-has-hover-border pi-full-height pi-hidden-sm">
<li class="pi-has-dropdown"><a href="<?php echo $url; ?>product.php"><span>Products</span></a>
    <ul class="pi-submenu pi-has-border pi-items-have-borders pi-has-shadow pi-submenu-dark">
		<li><a href="<?php echo $url; ?>product.php"><span>Products Overview</span></a></li>
		<li><a href="<?php echo $url; ?>high_level_product.php"><span>High Level Product Value Prop</span></a></li>
		<li><a href="<?php echo $url; ?>features.php"><span>Features</span></a></li>
	</ul>
</li>
<li class="pi-has-dropdown"><a href="javascript:void(0)" style="cursor:default"><span>Solutions</span></a>
	<ul class="pi-submenu pi-has-border pi-items-have-borders pi-has-shadow pi-submenu-dark">
		<!--<li><a href="<?php echo $url; ?>solution.php"><span>Solutions Overview</span></a></li>-->
		<li><a href="<?php echo $url; ?>solutions/virtualization.php"><span>Virtualization</span></a></li>
		<li><a href="<?php echo $url; ?>solutions/cloud.php"><span>Cloud</span></a></li>
        <li><a href="<?php echo $url; ?>solutions/bigdata.php"><span>Big Data</span></a></li>
        <li><a href="<?php echo $url; ?>solutions/industries.php"><span>Industries</span></a></li>
	</ul>
</li>
<li class="pi-has-dropdown"><a href="<?php echo $url; ?>company.php"><span>Company</span></a>
	<ul class="pi-submenu pi-has-border pi-items-have-borders pi-has-shadow pi-submenu-dark">
		<li><a href="<?php echo $url; ?>company.php"><span>Company Overview</span></a></li>
		<li><a href="<?php echo $url; ?>contact.php"><span>Contact Us</span></a></li>
	</ul>
</li>

</ul>
</div>
<!-- End menu -->

<!-- Mobile menu button -->
<div class="pi-header-block pi-pull-right pi-hidden-lg-only pi-hidden-md-only">
	<button class="btn pi-btn pi-mobile-menu-toggler" data-target="#pi-main-mobile-menu">
		<i class="icon-menu pi-text-center"></i>
	</button>
</div>
<!-- End mobile menu button -->


	<!-- Mobile menu -->
	<div id="pi-main-mobile-menu" class="pi-section-menu-mobile-w">
		<div class="pi-section-menu-mobile">

			<ul class="pi-menu-mobile pi-items-have-borders pi-menu-mobile-dark">
				<li><a href="<?php echo $url; ?>product.php"><span>Products</span></a>
                <ul>
					<li><a href="<?php echo $url; ?>product.php"><span>Products Overview</span></a></li>
					<li><a href="<?php echo $url; ?>high_level_product.php"><span>High Level Product Value Prop</span></a></li>
		            <li><a href="<?php echo $url; ?>features.php"><span>Features</span></a></li>
				</ul>
				</li>
				<li><a href="javascript:void(0)"><span>Solutions</span></a>
                <ul>
					<!--<li><a href="<?php echo $url; ?>solution.php"><span>Solutions Overview</span></a></li>-->
		            <li><a href="<?php echo $url; ?>solutions/virtualization.php"><span>Virtualization</span></a></li>
		            <li><a href="<?php echo $url; ?>solutions/cloud.php"><span>Cloud</span></a></li>
		            <li><a href="<?php echo $url; ?>solutions/bigdata.php"><span>Big Data</span></a></li>
		            <li><a href="<?php echo $url; ?>solutions/industries.php"><span>Industries</span></a></li>
				</ul>
				</li>
				<li><a href="<?php echo $url; ?>company.php"><span>Company</span></a>
				<ul>
					<li><a href="<?php echo $url; ?>company.php"><span>Company Overview</span></a></li>
		            <li><a href="<?php echo $url; ?>contact.php"><span>Contact Us</span></a></li>
				</ul>
			    </li>
            </ul>

		</div>
	</div>
	<!-- End mobile menu -->