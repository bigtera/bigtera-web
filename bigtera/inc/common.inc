<script src="<?php echo $url; ?>3dParty/jquery-1.11.0.min.js"></script>
<script src="<?php echo $url; ?>scripts/pi.easings.js"></script>
<script src="<?php echo $url; ?>scripts/pi.helpers.js"></script>
<script src="<?php echo $url; ?>scripts/pi.boundManager.js"></script>
<script src="<?php echo $url; ?>3dParty/inview.js"></script>
<script src="<?php echo $url; ?>3dParty/pi.init.animations.min.js"></script>
<script src="<?php echo $url; ?>scripts/pi.imagesLoader.js"></script>
<script src="<?php echo $url; ?>scripts/pi.columnFix.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.caption.js"></script>
<script src="<?php echo $url; ?>scripts/pi.slider.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.slider.js"></script>
<script src="<?php echo $url; ?>3dParty/gauge.min.js"></script>
<script src="<?php echo $url; ?>scripts/pi.counter.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.counter.js"></script>
<script src="<?php echo $url; ?>scripts/pi.parallax.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.parallax.js"></script>
<script src="<?php echo $url; ?>3dParty/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script src="<?php echo $url; ?>3dParty/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.revolutionSlider.js"></script>
<script src="<?php echo $url; ?>scripts/pi.ddMenu.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.removeLastElMargin.js"></script>
<script src="<?php echo $url; ?>scripts/pi.fixedHeader.js"></script>
<script src="<?php echo $url; ?>scripts/pi.mobileMenu.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.submitFormFooter.js"></script>
<script src="<?php echo $url; ?>scripts/pi.detectTransition.js"></script>
<script src="<?php echo $url; ?>scripts/pi.alert.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.formsBlurClasses.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.placeholder.js"></script>
<script src="<?php echo $url; ?>3dParty/colorbox/jquery.colorbox-min.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.colorbox.js"></script>
<script src="<?php echo $url; ?>3dParty/jquery.easing.1.3.js"></script>
<script src="<?php echo $url; ?>3dParty/jquery.scrollTo.min.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.jqueryScrollTo.js"></script>
<script src="<?php echo $url; ?>scripts/pi.scrollTopArrow.js"></script>
<script src="<?php echo $url; ?>3dParty/tweetie/tweetie.min.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.tweetie.js"></script>
<script src="<?php echo $url; ?>scripts/pi.tab.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.tab.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.social.js"></script>
<script src="<?php echo $url; ?>scripts/pi.table.js"></script>
<script src="<?php echo $url; ?>scripts/pi.tooltip.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.tooltip.js"></script>
<script src="<?php echo $url; ?>scripts/pi.init.submitFormContact.js"></script>
<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-554c8a753db28adf" async="async"></script>
<script>
  $(function(){
	$("#maps1, #maps2, #maps3").mouseover(function(){
		var N = $(this).attr("id").substr(0);
		$("#mapdiv").attr( "src" , "<?php echo $url; ?>img/" + N + ".jpg" );	
	});
  });

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61983852-1', 'auto');
  ga('send', 'pageview');
</script>
