<!-- Footer -->
<!-- Copyright area -->
<div class="pi-section-w pi-section-header-w pi-section-dark pi-border-top-light pi-section-header-lg-w">
	<div class="pi-section pi-section-header pi-section-header-lg pi-center-text-2xs pi-clearfix">

		<!-- Text -->
		<span class="pi-header-block pi-header-block-txt pi-hidden-xs pi-footertxt">Copyright &copy; <?php echo date("Y"); ?> <a href="<?php echo $url; ?>">Bigtera limited</a>. All Rights Reserved.</span>
		<!-- End text -->

	</div>
</div>
<!-- End copyright area -->
<div class="pi-scroll-top-arrow" data-scroll-to="0"></div>
<!-- End footer -->