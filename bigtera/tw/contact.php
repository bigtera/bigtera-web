﻿<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 聯絡我們";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">聯絡我們</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li><a href="<?php echo $url; ?>company.php">公司</a></li>
				<li>聯絡我們</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white piICheck piStylishSelect piSocials">
	<div class="pi-section">

		<div class="pi-row">

			<div class="pi-col-sm-6">
				<div id="maps2">
				<h2 class="h4 pi-has-bg pi-uppercase pi-letter-spacing pi-margin-bottom-30">
					China
				</h2>
				<ul class="pi-list-with-icons pi-list-big-margins pi-padding-bottom-20">
					<li><span class="pi-bullet-icon"><i class="icon-location"></i></span>Room A-1703, Boya International Center, No. 1, Lizezhongyi Road, Chaoyang District, Beijing 100102, P.R.C.</li>

					<li><span class="pi-bullet-icon"><i class="icon-mail"></i></span><a href="mailto:Sales@bigtera.com.cn">Sales@bigtera.com.cn</a></li>
					
					<li><span class="pi-bullet-icon"><i class="icon-phone"></i></span>+86-010-58692180</li>
					<li class="pi-margin-top-minus-15"><span class="pi-bullet-icon"><i class="icon-phone"></i></span>+86-010-58698170</li>
				</ul>
			    </div>
                
                <div id="maps1">
				<h2 class="h4 pi-has-bg pi-uppercase pi-letter-spacing pi-margin-bottom-30">
					Taiwan
				</h2>
				<ul class="pi-list-with-icons pi-list-big-margins pi-padding-bottom-20">
					<li><span class="pi-bullet-icon"><i class="icon-location"></i></span>6F., No.96, Minquan Rd., Xindian Dist., New Taipei City 23141, Taiwan<br>
					
					<li><span class="pi-bullet-icon"><i class="icon-mail"></i></span><a href="mailto:info@bigtera.com">info@bigtera.com</a></li>
					
					<li><span class="pi-bullet-icon"><i class="icon-phone"></i></span>+886-2-2508-0079</li>
					<li class="pi-margin-top-minus-15"><span class="pi-bullet-icon"><i class="icon-print"></i></span>+886-2-2218-1568</li>
				</ul>
				</div>

				<div id="maps3">
				<h2 class="h4 pi-has-bg pi-uppercase pi-letter-spacing pi-margin-bottom-30">
					Malaysia
				</h2>
				<ul class="pi-list-with-icons pi-list-big-margins pi-padding-bottom-10">
					<li><span class="pi-bullet-icon"><i class="icon-location"></i></span>Level 36, Menara Citibank 165 Jalan Ampang, 50450 Kuala Lumpur, Malaysia<br>
					
					<li><span class="pi-bullet-icon"><i class="icon-mail"></i></span><a href="mailto:sales@bigtera.com.my">sales@bigtera.com.my</a></li>
					
					<li><span class="pi-bullet-icon"><i class="icon-phone"></i></span>+60-3-2169-7424</li>
					<li class="pi-margin-top-minus-15"><span class="pi-bullet-icon"><i class="icon-mobile"></i></span>+60-111-0768735</li>
				</ul>
				</div>
			</div>

			<div class="pi-col-sm-6">
                <img id="mapdiv" src="<?php echo $url; ?>img/maps2.jpg" alt="">
			</div>

		</div>

	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>
