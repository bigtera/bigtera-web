<?php include_once('config.php'); ?>
<?php
$metaD = "全球首家用戶自定義的、新型的存儲平台.Bigtera VirtualStor™ 提供了全球首家提供獨創的軟件定義儲存平台，其匯聚，整合以及優化了所有的儲存資源（包括SAN、NAS），同時提供給IT管理人員幾乎無限制的彈性配置能力去分配和使用既有的儲存資源。";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w" style="background-image: url(<?php echo $url; ?>img_external/revolution-slider/landbg.jpg); background-size: cover; background-position: 50% 50%; color:#fff">

<!-- Row -->
<div class="pi-row pi-grid-small-margins">
<div class="pi-section pi-padding-top-20 pi-padding-bottom-30" style="overflow:hidden">
  <!-- Col 2 -->
  <div class="pi-col-sm-8 landtagline">
    <h3 class="lead-34 pi-letter-spacing pi-weight-400 pi-margin-bottom-20">全球首家用戶自定義的、新型的儲存平台</h3>
    <p class="lead-16 pi-margin-bottom-30">Bigtera VirtualStor™ 提供了全球首家提供獨創的軟體定義儲存平台，其匯聚，整合以及優化了所有的儲存資源（包括SAN、NAS），同時提供給IT管理人員幾乎無限制的彈性配置能力去分配和使用既有的儲存資源。</p>
  </div>
  <!-- End col 2 -->
  <!-- Col 2 -->
  <div class="pi-col-sm-4 pi-center pi-padding-top-10 animated flipInY delay">
    <img src="<?php echo $url; ?>img_external/revolution-slider/landimg.png" alt="">
  </div>
  <!-- End col 2 -->
</div>
</div>
<!-- End Row -->

</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->
	
<div class="pi-section-w pi-section-white">
	<div class="pi-section pi-padding-bottom-20">
		
		<!-- Row -->
		<div class="pi-row pi-grid-big-margins">
			
			<!-- Col 3 -->
			<div class="pi-col-xs-4 pi-padding-bottom-40">
			
				<div class="pi-icon-box-vertical pi-icon-box-vertical-icon-bigger pi-text-center animated" data-animation="bounce" data-animation-delay="1000">
				
					<div class="pi-icon-box-icon pi-icon-box-icon-circle pi-icon-box-icon-base" style="background: #eef1f1;">
						<img src="<?php echo $url; ?>img_external/icons/80px/product_1.png" alt="">
					</div>
					
					<h5 class="pi-weight-700 pi-uppercase pi-letter-spacing"><a href="<?php echo $url; ?>product.php#sds" class="pi-link-dark">軟體定義儲存（融存）</a></h5>

					<p class="pi-margin-bottom-10">
						你希望將現有的IT基礎設施轉型為軟體定義的資料中心嗎? 
					</p>
					
					<p>
						<a href="<?php echo $url; ?>product.php#sds">了解更多<i class="icon-right-open-mini pi-icon-right"></i></a>
					</p>
					
				</div>
				
			</div>
			<!-- End col 3 -->
			
			<!-- Col 3-->
			<div class="pi-col-xs-4 pi-padding-bottom-40">
			
				<div class="pi-icon-box-vertical pi-icon-box-vertical-icon-bigger pi-text-center animated" data-animation="bounce" data-animation-delay="1000">
				
					<div class="pi-icon-box-icon pi-icon-box-icon-circle pi-icon-box-icon-base" style="background: #eef1f1;">
						<img src="<?php echo $url; ?>img_external/icons/80px/product_2.png" alt="">
					</div>

					<h5 class="pi-weight-700 pi-uppercase pi-letter-spacing"><a href="<?php echo $url; ?>product.php#converger" class="pi-link-dark">虛擬化聚合儲存（聚存）</a></h5>

					<p class="pi-margin-bottom-10">
						你希望找到更高效的伺服器虛擬化以及桌面虛擬化解決方案嗎?
					</p>
					
					<p>
						<a href="<?php echo $url; ?>product.php#converger">了解更多<i class="icon-right-open-mini pi-icon-right"></i></a>
					</p>
					
				</div>
				
			</div>
			<!-- End col 3 -->
			
			<!-- Col 3 -->
			<div class="pi-col-xs-4 pi-padding-bottom-40">
			
				<div class="pi-icon-box-vertical pi-icon-box-vertical-icon-bigger pi-text-center animated" data-animation="bounce" data-animation-delay="1000">
				
					<div class="pi-icon-box-icon pi-icon-box-icon-circle pi-icon-box-icon-base" style="background: #eef1f1;">
						<img src="<?php echo $url; ?>img_external/icons/80px/product_3.png" alt="">
					</div>

					<h5 class="pi-weight-700 pi-uppercase pi-letter-spacing"><a href="<?php echo $url; ?>product.php#scaler" class="pi-link-dark">橫向擴展儲存（極存）</a></h5>

					<p class="pi-margin-bottom-10">
						你希望儲存系統效能能夠隨容量的擴展而持續增長嗎?
					</p>
					
					<p>
						<a href="<?php echo $url; ?>product.php#scaler">了解更多<i class="icon-right-open-mini pi-icon-right"></i></a>
					</p>
					
				</div>
				
			</div>
			<!-- End col 3 -->
			
		</div>
		<!-- End row -->
				
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>