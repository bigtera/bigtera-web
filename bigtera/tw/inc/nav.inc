<!-- Menu -->
<div class="pi-header-block pi-pull-right">
<ul class="pi-simple-menu pi-has-hover-border pi-full-height pi-hidden-sm">
<li class="pi-has-dropdown"><a href="<?php echo $url; ?>product.php"><span>產品</span></a>
    <ul class="pi-submenu pi-has-border pi-items-have-borders pi-has-shadow pi-submenu-dark">
		<li><a href="<?php echo $url; ?>product.php"><span>產品系列</span></a></li>
		<li><a href="<?php echo $url; ?>high_level_product.php"><span>高級產品特性</span></a></li>
		<li><a href="<?php echo $url; ?>features.php"><span>功能</span></a></li>
	</ul>
</li>
<li class="pi-has-dropdown"><a href="javascript:void(0)" style="cursor:default"><span>方案</span></a>
	<ul class="pi-submenu pi-has-border pi-items-have-borders pi-has-shadow pi-submenu-dark">
		<!--<li><a href="<?php echo $url; ?>solution.php"><span>解決方案</span></a></li>-->
		<li><a href="<?php echo $url; ?>solutions/virtualization.php"><span>虛擬化</span></a></li>
		<li><a href="<?php echo $url; ?>solutions/cloud.php"><span>雲儲存</span></a></li>
        <li><a href="<?php echo $url; ?>solutions/bigdata.php"><span>大數據</span></a></li>
        <li><a href="<?php echo $url; ?>solutions/industries.php"><span>產業</span></a></li>
	</ul>
</li>
<li class="pi-has-dropdown"><a href="<?php echo $url; ?>company.php"><span>公司</span></a>
	<ul class="pi-submenu pi-has-border pi-items-have-borders pi-has-shadow pi-submenu-dark">
		<li><a href="<?php echo $url; ?>company.php"><span>公司概述</span></a></li>
		<li><a href="<?php echo $url; ?>contact.php"><span>聯絡我們</span></a></li>
	</ul>
</li>

</ul>
</div>
<!-- End menu -->

<!-- Mobile menu button -->
<div class="pi-header-block pi-pull-right pi-hidden-lg-only pi-hidden-md-only">
	<button class="btn pi-btn pi-mobile-menu-toggler" data-target="#pi-main-mobile-menu">
		<i class="icon-menu pi-text-center"></i>
	</button>
</div>
<!-- End mobile menu button -->


	<!-- Mobile menu -->
	<div id="pi-main-mobile-menu" class="pi-section-menu-mobile-w">
		<div class="pi-section-menu-mobile">

			<ul class="pi-menu-mobile pi-items-have-borders pi-menu-mobile-dark">
				<li><a href="<?php echo $url; ?>product.php"><span>產品</span></a>
                <ul>
					<li><a href="<?php echo $url; ?>product.php"><span>產品系列</span></a></li>
					<li><a href="<?php echo $url; ?>high_level_product.php"><span>高級產品特性</span></a></li>
		            <li><a href="<?php echo $url; ?>features.php"><span>功能</span></a></li>
				</ul>
				</li>
				<li><a href="javascript:void(0)"><span>方案</span></a>
                <ul>
					<!--<li><a href="<?php echo $url; ?>solution.php"><span>解決方案</span></a></li>-->
		            <li><a href="<?php echo $url; ?>solutions/virtualization.php"><span>虛擬化</span></a></li>
		            <li><a href="<?php echo $url; ?>solutions/cloud.php"><span>雲儲存</span></a></li>
		            <li><a href="<?php echo $url; ?>solutions/bigdata.php"><span>大數據</span></a></li>
		            <li><a href="<?php echo $url; ?>solutions/industries.php"><span>產業</span></a></li>
				</ul>
				</li>
				<li><a href="<?php echo $url; ?>company.php"><span>公司</span></a>
				<ul>
					<li><a href="<?php echo $url; ?>company.php"><span>公司概述</span></a></li>
		            <li><a href="<?php echo $url; ?>contact.php"><span>聯絡我們</span></a></li>
				</ul>
			    </li>
            </ul>

		</div>
	</div>
	<!-- End mobile menu -->