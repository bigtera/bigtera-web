<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 功能";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">功能</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li><a href="<?php echo $url; ?>product.php">產品</a></li>
				<li>功能</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->
<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-50">
        
        <!-- Tabs navigation -->
		<ul class="pi-tabs-navigation pi-responsive-sm pi-tabs-ac">
			<li class="pi-active"><a href="#features">功能列表</a></li>
			<li><a href="#architecture">產品架構</a></li>
			<li><a href="#performance">性能參數</a></li>
			<li><a href="#resilience">系統高可用性</a></li>
			<li><a href="#protection">數據安全策略</a></li>
		</ul>
		<!-- End tabs navigation -->

        <!-- Tabs content -->
		<div class="pi-tabs-content pi-tabs-content-shadow">
			
		<!-- Tabs content item -->
		<div class="pi-tab-pane pi-active" id="features">

        <!-- Table 1 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">功能列表：所有功能</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能點</th>
						<th>描述</th>
						<th style="width:21%">優勢</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>
				
					<!-- Table row -->
					<tr class="rowhover">
						<td>橫向擴展儲存</td>
						<td>可通過增加擴展儲存空間和性能</td>
						<td>高性價比，無性能 / 容量擴展瓶頸</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>支持多中儲存類型</td>
						<td>支持多種標準協議的儲存類型:
					      <ul>
					        <li>SAN (iSCSI, FC)</li>
					        <li>NAS (NFS, CIFS)</li>
					        <li>對象儲存 (Amazon S3, OpenStack Swift)</li>
				          </ul>
				        </td>
						<td>高度靈活性 / 高資源利用率</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>服務質量控制</td>
						<td>管理員可設置應用的IO性能上限</td>
						<td>更好的資源管控方式</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>基於閃存的固態硬碟加速技術</td>
						<td>採用SSD固態硬碟緩存加速技術提升IOPS</td>
						<td>高性能(IOPS)數據訪問</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>實時數據副本</td>
						<td>通過實時副本機制(最大支持10副本)提升數據高可用性和訪問性能</td>
						<td>數據高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>遠程數據複製</td>
						<td>複製數據到異地集群</td>
						<td>數據高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>數據去重</td>
						<td>去除底層重複數據以減少儲存空間消耗</td>
						<td>高效使用儲存資源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>數據壓縮</td>
						<td>實施無損壓縮數據以減少儲存空間消耗</td>
						<td>高效使用儲存資源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>自動精簡配置</td>
						<td>通過規劃 / 分配超過實際儲存容量的虛擬儲存資源，實現儲存資源優化管理功能</td>
						<td>優化儲存資源使用</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>持續數據保護</td>
						<td>採用快照技術，實現分鐘級的數據備份和恢復機制</td>
						<td>集群高可用和數據安全</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>數據加密技術</td>
						<td>提供實時數據加密/解密功能(採用Intel AES-NI算法或256位秘鑰軟件加密算法)</td>
						<td>數據安全策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>糾刪碼</td>
						<td>提供節點間RAID的數據保護機制</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>儲存分層</td>
						<td>支持將不同性能的儲存資源分層，提供給不同性能需求的業務應用使用</td>
						<td>業務按需獲取儲存資源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>數據自動平衡</td>
						<td>數據儲存自動平衡到儲存集群各節點中</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>負載均衡</td>
						<td>數據訪問請求平衡到儲存集群各節點中</td>
						<td>集群性能優化</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>自我修復功能</td>
						<td>支持RAID技術，實時副本技術以及糾刪碼技術修復丟失或遭破壞的數據</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>雲端儲存與備份功能</td>
						<td>支持數據備份至Amazon S3(或類Amazon S3雲儲存)中</td>
						<td>數據保護策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>軟RAID</td>
						<td>在節點內提供RAID支持</td>
						<td>數據保護策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>即插即用</td>
						<td>儲存節點無需預先配置即可加入既有集群，實現不中斷任何業務情況下的集群無縫擴容</td>
						<td>數據保護策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>IP接管技術</td>
						<td>當儲存節點故障時，實現業務無縫接管和遷移</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>數據就近儲存</td>
						<td>數據被存放於更接近業務應用程度端，提升性能</td>
						<td>高效使用儲存資源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>數據遷移</td>
						<td>隨應用程式的使用，被納管的舊儲存設備上的數據自動遷移到VitualStor儲存集群中</td>
						<td>高效使用儲存資源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>去中心化架構</td>
						<td>3節點以上(包括3節點)集群部署規模，所有節點儲存資源被抽象，整合為單一的，巨大的儲存資源池。任何節點故障其業務將自動被其他節點接管，確保儲存服務可持續性</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>開放的管理API</td>
						<td>可通過管理API同客戶既有IT設備管理平台融合</td>
						<td>易於管理</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>監控和警告</td>
						<td>自動監控儲存設備和資源使用狀況，自動報警機制</td>
						<td>易於管理</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>系統高可用</td>
						<td>當集群中某台設備節點故障時，剩餘節點自動接管並均衡故障節點負載，消除單點故障</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>VAAI支持</td>
						<td>支持VMWare vSphere 儲存API——VAAI直接使用儲存硬體資源</td>
						<td>提升VMWare I/O性能</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>全局內存緩存技術</td>
						<td>使用節點空閒內存構建全局內存緩存池，加速數據訪問</td>
						<td>高性能(IOPS)數據訪問及集群高可用</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Amazon S3資源池</td>
						<td>為Amazon S3儲存創建資源池</td>
						<td>集群彈性管理機制</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>NAS設備整合</td>
						<td>支持NAS設備整合到儲存集群中</td>
						<td>集群彈性管理機制</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>SAN卷克隆技術</td>
						<td>支持從既有快照中克隆SAN數據卷</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>OSD雙活高可用模式</td>
						<td>支持兩台OSD節點以主備或雙活模式共同管理一個儲存卷</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>多路徑模式掛載外部儲存</td>
						<td>通過多路徑方式掛載外部SAN儲存，避免單路徑失效</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>緩存池</td>
						<td>可為較慢儲存池創建緩存池以加速數據訪問</td>
						<td>提升業務應用程式訪問性能</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>SAN(FC)訪問控制</td>
						<td>可配置FC SAN用戶訪問權限</td>
						<td>數據保護策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>集中日誌管理和通知</td>
						<td>主動監控儲存集群</td>
						<td>易於管理</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>集群狀態可視化儀表盤</td>
						<td>所有儲存節點狀態和資源使用狀況一覽圖</td>
						<td>易於管理</td>
					</tr>
					<!-- End table row -->
				
				</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="architecture">

        <!-- Table 2 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">架構</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能點</th>
						<th>描述</th>
						<th style="width:21%">優勢</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>
				
					<!-- Table row -->
					<tr class="rowclick link">
						<td>橫向擴展儲存</td>
						<td>可通過增加擴展儲存空間和性能</td>
						<td>高性價比，無性能 / 容量擴展瓶頸</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">橫向擴展架構</p>
		                  <p class="lead-14">
			              VirtualStor™ 橫向擴展架構意味著企業可以平滑的方式投資硬體設備，同時按需及時向數據中心添加儲存資源。有效避免過量購置，容量規劃等問題，使得成本持續可控。 
		                  </p>
		                  <p class="lead-14">
			              儲存節點可無縫加入到VirtualStor™ 儲存集群中而不影響任何在線應用程式和業務。隨著VirtualStor™ 的橫向擴展，不僅數據中心的容量會得到擴充，而且IO訪問頻寬以及IOPS性能也同步得到線性提升。這意味著儲存容量，數據傳輸和處理能力都會隨節點的橫向擴展而顯著提升。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>支持多中儲存類型</td>
						<td>支持多種標準協議的儲存類型
                          <ul>
                            <li>SAN (iSCSI, FC)</li>
                            <li>NAS (NFS, CIFS)</li>
                            <li>對象儲存(Amazon S3, Openstack Swift)</li>
                          </ul>
						</td>
						<td>高度靈活性 / 高資源利用率</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">多種儲存類型支持</p>
		                  <p class="lead-14">
			              VirtualStor™ 可同時支持多種儲存類型(SAN, NAS以及對象儲存)。這是通過支持多種儲存類型的標準訪問協議而實現的。這種能力賦予管理員更多的資源使用的靈活性，尤其是在整合型的基礎架構環境中。VirtualStor™ 支持以下儲存類型和協議：
			              <ul>
					        <li>SAN (iSCSI, FC)</li>
					        <li>NAS (NFS, CIFS)</li>
					        <li>對象儲存 (Amazon S3, OpenStack Swift)</li>
				          </ul>
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>基於閃存的固態硬碟加速技術</td>
						<td>採用SSD固態硬碟緩存加速技術提升IOPS</td>
						<td>高性能(IOPS)數據訪問</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">SSD固態硬碟加速</p>
		                  <p class="lead-14">
			              VirtualStor™ 可採用多種方式為數據中心提供極速的訪問性能。首先，通過SSD固態硬碟緩存技術，VirtualStor™ 可加提升據訪問和應用程式性能。當管理員啟用SSD固態硬碟加速功能，VirtualStor™ 將隨機訪問IO放到SSD固態硬碟中，而將順序寫入的IO操作放入機械硬碟中。結合SSD固態硬碟緩存技術和順序寫入技術，VirtualStor™ 可以改善傳統機械硬碟性能10倍以上。通過添加更多的SSD固態硬碟(SATA/SAS, PCIe)或擴展更多的VirtualStor™ 節點管理員可獲取更高的性能。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>糾刪碼</td>
						<td>提供節點間RAID的數據保護機制</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">糾刪碼</p>
		                  <p class="lead-14">
			              VirtualStor™ 將數據文件分割成多個小的數據塊(例如一個文件被分割成A,B,C和D四個數據塊)並將其分佈於儲存集群的各個節點中。當啟用糾刪碼技術時，會針對原始數據塊生成相應的校驗碼數據塊，當任何原始數據塊遭損毀或刪除，均可通過校驗碼數據塊重新恢復丟失或損毀的數據塊。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>數據自動平衡</td>
						<td>數據儲存自動平衡到儲存集群各節點中</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">數據平衡分佈</p>
		                  <p class="lead-14">
			              VirtualStor™ 將數據文件分割成多個小的數據塊(例如一個文件被分割成A,B,C和D四個數據塊)並將其分佈於儲存集群的各個節點中。VirtualStor™ 通過高效的算法確保了數據塊均勻分佈，從而避免了單節點儲存容量過載。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>負載均衡</td>
						<td>數據訪問請求平衡到儲存集群各節點中</td>
						<td>集群性能優化</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">負載均衡</p>
		                  <p class="lead-14">
			              數據訪問請求被均勻分佈於VirtualStor™ 的各個節點上。從而集群節點數越多，整個儲存集群的性能就越好。這也意味著隨VirtualStor™ 節點增加，IO訪問頻寬性能也隨之線性提升。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>自我修復功能</td>
						<td>支持RAID技術，實時副本技術以及糾刪碼技術修復丟失或遭破壞的數據</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">自我修復能力</p>
		                  <p class="lead-14">
			              VirtualStor™ 具備多種集群修復功能，而如何修復損傷取決於啟用了何種數據保護機制。
		                  </p>
		                  <p class="lead-14">
			              當啟用數據實時副本機制時，數據至少有兩份副本儲存於VirtualStor™ 集群的不同節點中。當VirtualStor™ 發現任何一台節點設備或是任何一個數據塊損壞，自動修復程序將被啟動，從另一份完整的數據塊中將複製一份新的數據副本並將其保存到另一個健康的儲存節點中。這一方案避免了設備的單點故障，同時也確保了關鍵數據的安全性。
		                  </p>
		                  <p class="lead-14">
			              糾刪碼採用了另一種方式確保數據安全。VirtualStor™ 將數據文件分割成多個小的數據塊(例如一個文件被分割成A,B,C和D四個數據塊)並將其分佈於儲存集群的各個節點中。當啟用糾刪碼技術時，會針對原始數據塊生成相應的校驗碼數據塊，當任何原始數據塊遭損毀或刪除，均可通過校驗碼數據塊重新恢復丟失或損毀的數據塊。而軟RAID作為另一種數據保護機制其原理於糾刪碼如出一轍，只是軟RAID作用範圍僅限於一台節點中而已。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>系統高可用性</td>
						<td>當集群中某台設備節點故障時，剩餘節點自動接管並均衡故障節點負載，消除單點故障</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">高可用性</p>
		                  <p class="lead-14">
			              部署VirtualStor™ 儲存集群至少需要3台節點設備。每台設備均等價地提供數據訪問能力的支撐。任何一台設備故障，IP接管技術均可將其負載和業務無縫遷移到其他健康的設備上，確保整個系統的高可用性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>即插即用</td>
						<td>儲存節點無需預先配置即可加入既有集群，實現不中斷任何業務情況下的集群無縫擴容</td>
						<td>數據保護策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">即插即用架構</p>
		                  <p class="lead-14">
			              VirtualStor™ 能自動偵測到新節點加入。新節點一旦加入，其儲存資源將自動無縫融合到既有的單一大塊儲存池中。隨後VirtualStor™ 開始重新平衡數據到儲存池中的每個節點中。高效的數據再平衡算法確保每次新節點的加入需要平衡和遷移的數據量最小。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>開放的管理API</td>
						<td>可通過管理API同客戶既有IT設備管理平台融合</td>
						<td>易於管理</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">開放的管理API</p>
		                  <p class="lead-14">
			              VirtualStor™ 提供了開放的管理API，管理員可使用該組API配置和管理整個VirtualStor™ 集群，同時可與現有的管理平台無縫整合。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->
				
				</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="performance">

        <!-- Table 3 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">性能</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能點</th>
						<th>描述</th>
						<th style="width:21%">優勢</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>橫向擴展儲存</td>
						<td>可通過增加擴展儲存空間和性能</td>
						<td>高性價比，無性能 / 容量擴展瓶頸</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">橫向擴展架構</p>
		                  <p class="lead-14">
			              VirtualStor™ 橫向擴展架構意味著企業可以平滑的方式投資硬體設備，同時按需及時向數據中心添加儲存資源。有效避免過量購置，容量規劃等問題，使得成本持續可控。
儲存節點可無縫加入到VirtualStor™ 儲存集群中而不影響任何在線應用程式和業務。隨著VirtualStor™ 的橫向擴展，不僅數據中心的容量會得到擴充，而且IO訪問頻寬以及IOPS性能也同步得到線性提升。這意味著儲存容量，數據傳輸和處理能力都會隨節點的橫向擴展而顯著提升。

		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>服務質量控制</td>
						<td>管理員可設置應用的IO性能上限</td>
						<td>更好的資源管控方式</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">服務質量管控(QoS)</p>
		                  <p class="lead-14">
			              管理員可使用VirtualStor™ 中自帶的虛擬儲存器功能為不同的用戶配置不同儲存訪問策略。策略中可指定儲存容量、儲存類型、QoS、可訪問性以及高可用性級別等。虛擬儲存器的QoS包括三個指標：IOPS、儲存訪問頻寬以及儲存訪問延遲。管理員可指定虛擬儲存器的訪問頻寬以及IOPS上限，而儲存訪問延時會根據應用程式的數據訪問請求自動優化。同時高性能SSD閃存加速卡可有效提升IOPS，而橫向擴展儲存節點可同時提升IOPS以及訪問頻寬。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>基於閃存的固態硬碟加速技術</td>
						<td>採用SSD固態硬碟緩存加速技術提升IOPS</td>
						<td>高性能(IOPS)數據訪問</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">SSD固態硬碟加速</p>
		                  <p class="lead-14">
			              VirtualStor™ 可採用多種方式為數據中心提供極速的訪問性能。首先，通過SSD固態硬碟緩存技術，VirtualStor™ 可加速提升數據訪問和應用程式性能。當管理員啟用SSD固態硬碟加速功能，VirtualStor™ 將隨機訪問IO放到SSD固態硬碟中，而將順序寫入的IO操作放入機械硬碟中。結合SSD固態硬碟緩存技術和順序寫入技術，VirtualStor™ 可以改善傳統機械硬碟性能10倍以上。通過添加更多的SSD固態硬碟(SATA/SAS, PCIe)或擴展更多的VirtualStor™ 節點管理員可獲取更高的性能。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>儲存分層</td>
						<td>支持將不同性能的儲存資源分層，提供給不同性能需求的業務應用使用</td>
						<td>業務按需求獲取儲存資源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">儲存分層</p>
		                  <p class="lead-14">
			              VirtualStor™ 可以為不同的業務應用程式指定其訪問的儲存類型。這意味著那些需要高性能IO的應用程式和數據可以存放於閃存固態硬碟中，而其他應用程式所訪問的數據放置於傳統的機械硬碟中，而數據歸檔則可以放置於雲儲存中(Amazon S3，OpenStack Swift)。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>數據就近儲存</td>
						<td>數據被存放於更接近業務應用程度端，提升性能</td>
						<td>高效使用儲存資源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">數據就近儲存</p>
		                  <p class="lead-14">
			              VirtualStor™ 將數據文件分割成多個小的數據塊。這些數據塊被VirtualStor™ 順序儲存於各個節點設備上。隨著應用程式的訪問，VirtualStor™ 可自動將應用程式頻繁訪問的數據塊移動至距離應用程式更近的位置，提供了更好的數據訪問性能。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>VAAI支持</td>
						<td>支持VMWare vSphere 儲存API——VAAI直接使用儲存硬體資源</td>
						<td>提升VMWare I/O性能</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>全局內存緩存技術</td>
						<td>使用節點空閒內存構建全局內存緩存池，加速數據訪問</td>
						<td>高性能(IOPS)數據訪問及集群高可用</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>緩存池</td>
						<td>可為較慢儲存池創建緩存池以加速數據訪問</td>
						<td>提升業務應用程式訪問性能</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="resilience">

        <!-- Table 4 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">系統高可用性</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能點</th>
						<th>描述</th>
						<th style="width:21%">優勢</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>負載均衡</td>
						<td>數據訪問請求平衡到儲存集群各節點中</td>
						<td>集群性能優化</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">負載均衡</p>
		                  <p class="lead-14">
			              數據訪問請求被均勻分佈於VirtualStor™ 的各個節點上。從而集群節點數越多，整個儲存集群的性能就越好。這也意味著隨VirtualStor™ 節點增加，IO訪問頻寬性能也隨之線性提升。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>自我修復功能</td>
						<td>支持RAID技術，實時副本技術以及糾刪碼技術修復丟失或遭破壞的數據</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">自我修復能力</p>
		                  <p class="lead-14">
			              VirtualStor™ 具備多種集群修復功能，而如何修復損傷取決於啟用了何種數據保護機制。
		                  </p>
		                  <p class="lead-14">
			              當啟用數據實時副本機制時，數據至少有兩份副本儲存於VirtualStor™ 集群的不同節點中。當VirtualStor™ 發現任何一台節點設備或是任何一個數據塊損壞，自動修復程序將被啟動，從另一份完整的數據塊中將複製一份新的數據副本並將其保存到另一個健康的儲存節點中。這一方案避免了設備的單點故障，同時也確保了關鍵數據的安全性。
		                  </p>
		                  <p class="lead-14">
			              糾刪碼採用了另一種方式確保數據安全。VirtualStor™ 將數據文件分割成多個小的數據塊(例如一個文件被分割成A,B,C和D四個數據塊)並將其分佈於儲存集群的各個節點中。當啟用糾刪碼技術時，會針對原始數據塊生成相應的校驗碼數據塊，當任何原始數據塊遭損毀或刪除，均可通過校驗碼數據塊重新恢復丟失或損毀的數據塊。而軟RAID作為另一種數據保護機制其原理於糾刪碼如出一轍，只是軟RAID作用範圍僅限於一台節點中而已。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>系統高可用性</td>
						<td>當集群中某台設備節點故障時，剩餘節點自動接管並均衡故障節點負載，消除單點故障</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">高可用性</p>
		                  <p class="lead-14">
			              部署VirtualStor™ 儲存集群至少需要3台節點設備。每台設備均等價地提供數據訪問能力的支撐。任何一台設備故障，IP接管技術均可將其負載和業務無縫遷移到其他健康的設備上，確保整個系統的高可用性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>即插即用</td>
						<td>儲存節點無需預先配置即可加入既有集群，實現不中斷任何業務情況下的集群無縫擴容</td>
						<td>數據保護策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">即插即用架構</p>
		                  <p class="lead-14">
			              VirtualStor™ 能自動偵測到新節點加入。新節點一旦加入，其儲存資源將自動無縫融合到既有的單一大塊儲存池中。隨後VirtualStor™ 開始重新平衡數據到儲存池中的每個節點中。高效的數據再平衡算法確保每次新節點的加入需要平衡和遷移的數據量最小。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>IP接管技術</td>
						<td>當儲存節點故障時，實現業務無縫接管和遷移</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">IP接管技術</p>
		                  <p class="lead-14">
			              部署VirtualStor™ 儲存集群至少需要3台節點設備。每台設備均等價地提供數據訪問能力的支撐。任何一台設備故障，IP接管技術均可將其負載和業務無縫遷移到其他健康的設備上，確保整個系統的高可用性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>去中心化架構</td>
						<td>3節點以上(包括3節點)集群部署規模，所有節點儲存資源被抽象，整合為單一的，巨大的儲存資源池。任何節點故障其業務將自動被其他節點接管，確保儲存服務可持續性</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">去中心化管理平台</p>
		                  <p class="lead-14">
			              由於VirtualStor™ 提供了一個統一的管理平台管理不同類型的儲存資源（包括SAN, NAS以及CAS），因而保留了其一貫的簡單性管理風格。只要VirtualStor™ 集群中有任意一台節點可用，管理平台即可訪問。管理平台提供了一個可視化儀表盤全局監控和管理儲存設備以及儲存資源的事情情況。管理員亦可設置警告策略實現對儲存資源和集群的主動式監控。
		                  </p>
		                  <p class="lead-14">
			              VirtualStor™ 管理平台賦予管理員全權掌控儲存資源的能力。管理員可直觀地為每個虛擬儲存器配置儲存類型(NAS, SAN, CAS)，容量，QoS(IOPS以及儲存訪問頻寬)，數據高可用級別(針對業務數據)以及數據服務(數據壓縮，數據去重，數據加密)等。VirtualStor™ 同時也提供開放的restful管理API，使得管理員可將其無縫整合到既有的IT管理平台中。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>SAN卷克隆技術</td>
						<td>支持從既有快照中克隆SAN數據卷</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>OSD雙活高可用模式</td>
						<td>支持兩台OSD節點以主備或雙活模式共同管理一個儲存卷</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>多路徑模式掛載外部儲存</td>
						<td>通過多路徑方式掛載外部SAN儲存，避免單路徑失效</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="protection">

        <!-- Table 5 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">數據保護策略</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能點</th>
						<th>描述</th>
						<th style="width:21%">優勢</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>數據實時副本</td>
						<td>通過實時副本機制(最大支持10副本)提升數據高可用性和訪問性能</td>
						<td>數據高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">數據實時副本</p>
		                  <p class="lead-14">
			              VirtualStor™ 將數據文件分割成小的數據塊並將這些數據塊分發到儲存集群的各個節點上進行管理，數據塊至少留存兩份副本，且數據塊副本被保存於不同的物理節點中已提供數據高可用能力。管理員可按需為數據塊創建最多10份副本，而副本數越多，數據塊的安全級別則越高。副本數可基於業務應用或虛擬儲存器進行配置。更多的副本數亦可進一步提升訪問性能，因為VirtualStor™ 的智能監控體系明確知道哪些數據塊正在被使用，而當新業務請求到達是，將其定向到那些尚未有業務或負載較輕的數據塊副本上。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>遠程數據複製</td>
						<td>複製數據到異地集群</td>
						<td>數據高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">遠程數據複製</p>
		                  <p class="lead-14">
			              管理員可配置異步複製集群中的數據到異地集群，這是異地容災的典型方案。例如，公司總部可能希望數據複製到分部進行備份，或是多個分部希望將數據匯聚到公司總部進行保存等。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>持續數據保護</td>
						<td>採用快照技術，實現分鐘級的數據備份和恢復機制</td>
						<td>集群高可用和數據安全</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">持續數據保護</p>
		                  <p class="lead-14">
			              VirtualStor™ 會持續監控儲存於集群中的數據。管理員可配置粒度達分鐘級的週期性快照策略。這使得管理員可以隨時進行數據的備份和恢復。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>數據加密技術</td>
						<td>提供實時數據加密/解密功能(採用Intel AES-NI算法或256位秘鑰軟件加密算法)</td>
						<td>數據安全策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">數據加密技術</p>
		                  <p class="lead-14">
			              VirtualStor™ 對於S3儲存資源池中的數據可提供Intel AES-NI加密技術或長達256位秘鑰的加密算法以確保數據安全性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>糾刪碼</td>
						<td>提供節點間RAID的數據保護機制</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">糾刪碼</p>
		                  <p class="lead-14">
			              VirtualStor™ 將數據文件分割成多個小的數據塊(例如一個文件被分割成A,B,C和D四個數據塊)並將其分佈於儲存集群的各個節點中。當啟用糾刪碼技術時，會針對原始數據塊生成相應的校驗碼數據塊，當任何原始數據塊遭損毀或刪除，均可通過校驗碼數據塊重新恢復丟失或損毀的數據塊。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>數據自動平衡</td>
						<td>數據儲存自動平衡到儲存集群各節點中</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">數據平衡分佈</p>
		                  <p class="lead-14">
			              VirtualStor™ 將數據文件分割成多個小的數據塊(例如一個文件被分割成A,B,C和D四個數據塊)並將其分佈於儲存集群的各個節點中。VirtualStor™ 通過高效的算法確保了數據塊均勻分佈，從而避免了單節點儲存容量過載。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>自我修復功能</td>
						<td>支持RAID技術，實時副本技術以及糾刪碼技術修復丟失或遭破壞的數據</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">自我修復能力</p>
		                  <p class="lead-14">
			              VirtualStor™ 具備多種集群修復功能，而如何修復損傷取決於啟用了何種數據保護機制。
		                  </p>
		                  <p class="lead-14">
			              當啟用數據實時副本機制時，數據至少有兩份副本儲存於VirtualStor™ 集群的不同節點中。當VirtualStor™ 發現任何一台節點設備或是任何一個數據塊損壞，自動修復程序將被啟動，從另一份完整的數據塊中將複製一份新的數據副本並將其保存到另一個健康的儲存節點中。這一方案避免了設備的單點故障，同時也確保了關鍵數據的安全性。
		                  </p>
		                  <p class="lead-14">
			              糾刪碼採用了另一種方式確保數據安全。VirtualStor™ 將數據文件分割成多個小的數據塊(例如一個文件被分割成A,B,C和D四個數據塊)並將其分佈於儲存集群的各個節點中。當啟用糾刪碼技術時，會針對原始數據塊生成相應的校驗碼數據塊，當任何原始數據塊遭損毀或刪除，均可通過校驗碼數據塊重新恢復丟失或損毀的數據塊。而軟RAID作為另一種數據保護機制其原理於糾刪碼如出一轍，只是軟RAID作用範圍僅限於一台節點中而已。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>雲端備份與恢復功能</td>
						<td>支持數據備份至Amazon S3(或類Amazon S3雲儲存)中</td>
						<td>數據保護策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">雲端備份與恢復功能</p>
		                  <p class="lead-14">
			              雲儲存為業務提供了最高性價比的儲存服務（就容量而言）。VirtualStor™允許管理員將數據備份到雲端或從雲端恢復到本地集群中來。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>數據遷移</td>
						<td>隨應用程式的使用，被納管的舊儲存設備上的數據自動遷移到VitualStor儲存集群中</td>
						<td>高效使用儲存資源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">數據遷移</p>
		                  <p class="lead-14">
			              VirtualStor™ 可從其管理的儲存資源中創建一個無縫過渡儲存池用於數據遷移。隨著業務應用程式對舊有儲存設備中數據的訪問，數據將被無縫遷移到新的VirtualStor™ 儲存集群中。這一無縫遷移能力意味著陳舊的或受損的儲存設備可被在線汰換而不會影響到既有的應用，確保了業務的可持續性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>SAN(FC)訪問控制</td>
						<td>可配置FC SAN用戶訪問權限</td>
						<td>數據保護策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>