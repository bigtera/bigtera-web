<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - 雲儲存";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(../img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">雲儲存</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li><a href="">解決方案</a></li>
				<li>雲儲存</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-text-base">升級您的公有雲和混合雲解決方案</p>
		<p class="lead-14">
			成本控制是企業轉向雲儲存和雲服務的一個主要原因。因為雲架構為儲存和計算資源提供了最高性價比的解決方案。Bigtera在提供更安全，更高性價比的雲平台解決方案的同時更能夠大幅提升公有雲和企業私有雲的性能。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>增加投資回報率：使您的雲儲存進一步具備數據壓縮和數據去重能力。數據壓縮可進一步提升數據儲存密度而數據去重在底層通過高效算法刪除重複數據塊，兩者均可顯著減少數據儲存時的空間消耗，提升雲儲存的可用容量。</li>
						<li>確保數據安全：數據安全是確保業務持續運行的基礎架構中非常關鍵的部分。糾刪碼、數據加密技術、遠程雲端數據複製技術均為確保數據安全和業務的可持續性的有效手段。Bigtera產品可通過Intel AES-NI加密技術為雲儲存數據提供加密服務。同樣Bigtera產品中內建的糾刪碼技術通過將數據文件分割為多個小的數據塊，並將其分佈存放於集群的各節點中，同時為數據塊建構校驗碼塊，確保任意數據塊丟失或損壞，依舊可通過校驗碼塊重新生成損壞或丟失的數據塊。最後Bigtera產品允許將企業私有雲或儲存基礎架構中的數據通過Amazon S3或OpenStack Swift協議備份到共有雲上。實現多級數據安全機制。</li>
						<li>高性能解決方案：數據訪問效率可能對雲平台的交互能力產生巨大影響，Bigtera能使您更靈活地控制數據流入、流出雲平台的頻寬。</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>