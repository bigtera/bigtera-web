<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - 虛擬化應用";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">虛擬化應用</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li><a href="">解決方案</a></li>
				<li>虛擬化應用</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-text-base">建構可伸縮的，靈活的和高效的虛擬化應用基礎架構</p>
		<p class="lead-14">
			大多數企業已經開始逐步將其數據中心基礎設施從物理機向虛擬機遷移。這一遷移源自企業CIO們希望進一步提高基礎架構的投資回報率。這一目標通過將整合物理服務器計算資源，在一台物理服務器中運行多台虛擬機，從而進一步提升計算和儲存資源的利用率來達成。
		</p>
		<p class="lead-14">
			隨著私有雲技術，大數據應用和解決方案的推廣，數據和應用程式的需求呈爆炸式增長。CIO們正在不斷尋找最優性價比的方案擴展其數據中心以滿足業務需求。而超聚合儲存和軟件定義儲存漸漸成為解決這一問題的關鍵所在。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>整合和聚合：整合儲存資源，將儲存和計算資源聚合到一個資源池中，為資源分配和管理提供了很大的靈活度。而支持橫向/縱向擴展計算和儲存資源正是Bigtera產品的優勢所在。</li>
						<li>提升用戶滿意度：隨著計算資源和儲存資源的高度聚合，管理員可指定儲存訪問的QoS(IOPS以及儲存訪問頻寬)。這為關鍵應用和數據提供了更為彈性的資管規劃能力。同時結合儲存分層技術，可確保關鍵業務和數據的高優先級以及高性能訪問，從而保障了用戶的使用滿意度。</li>
						<li>高效率儲存：VirtualStor™ 提供了一系列的數據服務使得儲存資源的利用率最大化。首先是自動精簡配置功能。自動精簡配置可提供超出物理容量的儲存資源給業務系統，儲存資源將按業務所需即時分配。數據壓縮和去重功能進一步提升了儲存資源使用效率。壓縮技術提升了儲存密度而去重技術消除了底層的重複數據塊從而從有限的物理空間中攫取出更大的儲存能力。</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>