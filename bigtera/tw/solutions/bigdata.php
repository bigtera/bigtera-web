<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - 大數據";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">大數據應用</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li><a href="">解決方案</a></li>
				<li>大數據應用</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled" style="padding-bottom:100px">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-text-base">Bigtera為大數據應用提供最好的基礎架構</p>
		<p class="lead-14">
			業務對大數據感興趣出於多種原因。主要原因在於通過大數據分析可為商業決策提供良好的數據支撐。而Bigtera產品可以幫助公司解決在處理大數據是遇到的重大問題。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>高IOPS需求問題：雲計算應用程式需要分析和可視化大量數據的處理能力，而Bigtera支持的基於閃存SSD加速的特性可為大數據計算提供足夠的性能支撐。</li>
						<li>高頻寬需求問題：數據傳輸性能在大數據計算和儲存過程中同樣非常關鍵。所有的Bigtera產品均可通過其優異的可伸縮性特性，為大數據應用提供所需的數據傳輸能力。</li>
						<li>容量性價比問題：Bigtera產品的可伸縮性為大數據應用儲存提供了最優性價比的解決方案。當Bigtera產品部署於企業數據中心時，管理員可在不中斷、不影響現有程式運行的情況下按需求逐步擴展儲存基礎架構。因此投資是細粒度的以及按需分配的。</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>