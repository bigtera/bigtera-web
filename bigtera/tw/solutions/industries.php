<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - 行業方案";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">行業方案</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li><a href="">解決方案</a></li>
				<li>行業方案</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-text-base">串流媒體</p>
		<p class="lead-14">
			串流媒體服務公司的儲存需求特性為：必須儲存大量的媒體數據並提供足夠的訪問性能以供其客戶通過流模式訪問媒體文件。<br>這些公司可採用Bigtera產品不斷擴容來滿足日益增長的儲存需求。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>高性能（訪問頻寬）需求：數據傳輸性能是串流媒體應用中的關鍵指標。Bigtera旗下所有產品均可通過橫向擴展的架構線性提升儲存訪問頻寬以及數據傳輸性能。</li>
						<li>橫向擴展架構：隨著媒體文件的日益增長，串流媒體公司所需的儲存資源也隨之增長。Bigtera系列產品的橫向擴展架構為串流媒體公司提供了一種細粒度，按業務增長逐步投資儲存基礎架構的解決方案。</li>
						<li>可定義的QoS：客戶對於串流媒體公司服務有一定的服務質量要求。Bigtera系列產品均支持管理員為每個虛擬儲存器指定儲存訪問的QoS（包括IOSP和儲存訪問頻寬），完美契合了串流媒體服務的需求。</li>
					</ul>
				</div>
			</div>
		</p>
		<p class="lead-26 pi-text-base">視頻監控</p>
		<p class="lead-14">
			視頻監控行業似乎和串流媒體行業面臨同樣的儲存服務需求。<br>隨著NVR捕獲的視頻內容不斷增漲，需要儲存設備提供持續穩定的儲存訪問頻寬和數據傳輸性能。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>橫向擴展架構：隨著視頻監控文件的持續增長，Bigtera系列產品的橫向擴展架構為視頻監控服務提供了一種細粒度，按業務增長逐步投資儲存基礎架構的解決方案。</li>
						<li>高性能（訪問頻寬）需求：數據傳輸性能是儲存大量NVR視頻監控流的關鍵指標。Bigtera旗下所有產品均可通過橫向擴展的架構線性提升儲存訪問頻寬以及數據傳輸性能。</li>
						<li>可定義的QoS：大量NVR視頻監控流並發寫入對儲存系統提出了一定的服務質量要求。Bigtera系列產品均支持管理員為每個虛擬儲存器指定儲存訪問的QoS（包括IOSP和儲存訪問頻寬），完美契合了視頻監控領域的需求。</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>