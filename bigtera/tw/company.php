<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 公司";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">公司概述</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li>公司</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-40">
		<p class="lead-26 pi-text-base">關於 Bigtera</p>
		<p class="lead-14">
			Bigtera是軟體定義儲存平台和解決方案業界領先且具顛覆性的創新者。Bigtera解決方案和儲存平台提供了非常高的性能以及可用性。提供幾乎無限的橫向擴展能力，在儲存容量擴展的同時也線性提升整個儲存集群的性能，同時為客戶提供了按業務增長逐步擴展基礎設置的能力。
		</p>
		<p class="lead-14">
			Bigtera的使命就是將企業基礎架構引入到軟體定義的自由世界中。因為我們發現這樣一個事實：目前的企業資料中心儲存基礎架構難以滿足大規模資料應用以及雲計算的需求。目前的資料中心基礎架構由於其使用傳統儲存的局限性，造成儲存設備和業務應用往往不得不一一對應來滿足企業需求。這導致在使用儲存設備時，產生大量的儲存孤島，也使得傳統儲存在維運方面缺乏靈活性。
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			企業CIO和IT工作者們已經開始考慮擁抱軟體定義基礎架構了(SDI)，但是他們也在擔心從現有基礎架構遷移到軟體定義模式下可能帶來的一系列問題和影響。我們知道改變一個企業的基礎架構不像是撥動開關那麼簡單，因此我們致力於幫助企業構建一個更加自由的軟體定義儲存基礎架構，通過顛覆性的創新技術，通過以下的核心理念幫助企業從現有的IT基礎架構平滑無縫地過渡到軟體定義儲存的基礎架構中。
		</p>
		<!-- Openings -->
		<div class="pi-tabs-vertical pi-responsive-sm">
			<ul class="pi-tabs-navigation pi-tabs-navigation-transparent" id="myTabs2">
				<li class="pi-active"><a href="#consolidation"><i class="icon-layout"></i>整合</a></li>
				<li><a href="#automation"><i class="icon-arrows-ccw"></i>自動化</a></li>
				<li><a href="#optimization"><i class="icon-gauge"></i>優化</a></li>
			</ul>
			<div class="pi-tabs-content pi-tabs-content-transparent">

				<div class="pi-tab-pane pi-active" id="consolidation">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">整合</span>
					</h3>
					<p class="lead-14">
						整合儲存資源是關鍵的第一步。我們花費了巨大的努力去整合儲存資源(SAN, NAS, DAS)以及雲儲存服務(Amazon S3、OpenStack Swift)。一旦企業需要整合現有資源，我們會關注在企業在更新或過渡到新的儲存架構過程中，如何盡可能多地保持和利用現有基礎架構中的儲存資源。這不僅降低了總擁有成本(TCO)、提高了投資回報率(ROI)，同時也為基礎架構的轉型提供足夠騰挪的空間。
					</p>
				</div>


				<div class="pi-tab-pane" id="automation">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">自動化</span>
					</h3>
					<p class="lead-14">
						資料遷移，資源配置以及資料恢復都是耗時耗力的工作，企業資料中心管理員往往需要花費大量時間處理上述類似的低附加值的工作，同時還希望能夠用較少的資源提供更多的服務。通過將低附加值的任務通過基礎架構自動完成，從而將資料中心維運團隊組織架構從被動型組織轉化為更為主動以及更加面向策略的組織架構，真正實現更少的資源，更大的能力。
					</p>
				</div>

				<div class="pi-tab-pane" id="optimization">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">優化</span>
					</h3>
					<p class="lead-14">
						自適應儲存系統可實時優化系統資源，並實現儲存資源利用率的最大化。具備自我學習且能夠對儲存基礎架構使用情況進行預測分析的能力，將幫助資料中心管理員預先為儲存和計算需求準備資源以及調整策略，確保整個企業基礎架構的平穩運行。
					</p>
				</div>

			</div>
		</div>
		<!-- End openings -->
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>