<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 高級產品特性";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-12 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">高級產品特性</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li><a href="<?php echo $url; ?>product.php">產品</a></li>
				<li>高級產品特性</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->
<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-50">
        
        <!-- Tabs navigation -->
		<ul class="pi-tabs-navigation pi-responsive-sm pi-tabs-ac">
			<li class="pi-active"><a href="#scaleout">橫向擴展架構</a></li>
			<li><a href="#performance">高性能</a></li>
			<li><a href="#unified">統一儲存</a></li>
			<li><a href="#efficient">高效儲存</a></li>
			<li><a href="#resilience">高可用性</a></li>
			<li><a href="#flexibility">高度靈活性</a></li>
		</ul>
		<!-- End tabs navigation -->

        <!-- Tabs content -->
		<div class="pi-tabs-content pi-tabs-content-shadow">
			
		<!-- Tabs content item -->
		<div class="pi-tab-pane pi-active" id="scaleout">

        <p class="lead-26 pi-text-base">隨業務增長逐步投資</p>
		<p class="lead-14">
			近年來，隨著企業數據的迅速膨脹，儲存業務響應要求越來越快，如何找到一個有效且高效的儲存解決方案已經成為業界IT管理員們共同面臨的巨大挑戰。針對上述的各個問題，最大的挑戰莫過於數據量的暴漲湮沒了儲存基礎架構能力。針對大量數據的爆發式增長，儲存基礎架構必須能夠儲存和提供基於如此大量數據的業務訪問能力。下一個問題就是如何處理內部用戶對於儲存服務的快速膨脹的需求。隨著IT需求的持續增長，這將是一場用不停息的戰爭。最終傳統儲存處理大規模數據所需的高昂代價將無法維持IT預算跟上業務數據不斷增長的需求。因為傳統儲存縱向擴展方案意味著IT基礎架構在建設初期就不得不為未來儲存使用需求，投入大量資金採購過量儲存空間和更高性能的儲存機頭；同時當未來業務出現變化，一旦超出初期容量及性能規劃，傳統儲存擴展時的設備成本、數據遷移成本以及管理成本將成指數級增長。
		</p>
		<p class="lead-14">
			Bigtera的使命就是允許企業享受軟件定義的自由世界。我們的使命源於我們瞭解目前的數據中心基礎設施難以處理大數據和雲計算相關的儲存需求。目前數據中心的基礎架構約束了儲存設備以及儲存資源不得不同業務系統一一對應，這導致了儲存使用時產生大量的信息孤島，這也是在使用傳統儲存時不夠彈性的地方。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			CIO和IT從業者們已經開始擁抱軟件定義儲存的基礎架構的概念了，但是他們還在擔心如何從既有的基礎架構過渡到期望的軟件定義儲存架構中。我們理解改變企業的儲存基礎架構並不像撥動開關那樣簡單，因此Bigtera致力於構建企業用戶可自由定義的儲存基礎架構，而我們的團隊將所有的精力和信念都投入到創建一個顛覆性的軟件定義儲存平台，以幫助企業用戶可以無縫地將現有的IT基礎架構平滑過渡到新一代軟件定義的數據中心架構中。
		</p>
        
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="performance">

        <p class="lead-26 pi-text-base">加速IT基礎設施</p>
		<p class="lead-14">
			數據中心不僅需要滿足儲存容量的需求，同時也要提供足夠的電力以及儲存訪問頻寬供業務和應用程序使用。如今很多數據中心都需要支持各種類型的高性能應用程序的負載，包括，但不僅限於，服務器虛擬化，桌面虛擬化以及對SQL數據庫應用的支持。因此處理、儲存以及發佈數據服務已不僅僅是容量方面的問題，而是需要同時提供更高的儲存服務性能方面的支撐。然而傳統儲存目前僅能勉強解決儲存容量的問題，仍需要尋求新的技術來處理性能和容量同步增長的需求。
		</p>
		<p class="lead-14">
			VirtualStor™ 引入多種方法同時對數據中心的儲存系統進行加速。首先利用SSD固態硬碟緩存技術，VirtualStor™ 大幅提升負載處理能力以及應用程序訪問性能。當管理員啟用SSD固態硬碟加速技術，VirtualStor™ 先將隨機IO訪問數據到SSD固態硬碟緩存中，進而整合其中的隨機IO訪問請求成為連續IO訪問，並將其順序寫入底層機械硬碟中。結合SSD固態硬碟緩存和順序寫入技術，VirtualStor™ 可提升傳統機械硬碟儲存性能達10倍以上。管理員更可增加更多的SSD(SATA/SAS, PCIe接口均可支持)或橫向擴展VirtualStor™ Scaler節點來進一步提升性能。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			隨著VirtualStor™ Scaler的橫向擴展，整個系統的IO訪問頻寬以及IOPS性能均可得到顯著提升。IOPS是指每秒可處理的IO請求數而IO訪問頻寬則反映了數據讀寫的速度。VirtualStor™ 集群中節點數越多，其處理和響應業務的速度就越快。VirtualStor™ 集群（VirtualStor™ 一體機）甚至可以通過執行集群內部事務以及利用集群中各節點的空閒內存構建全局內存緩存池，提升更高的儲存性能。管理員亦可為某些較慢的儲存介質指定緩存池來加速熱點數據訪問。VirtualStor™ 分佈式文件儲存架構配合實時數據副本機制，同樣也能夠提升數據讀取的訪問性能，因為VirtualStor™ 系統可根據儲存訪問請求智能定位到最近可訪問的數據塊副本上。
		</p>

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="unified">

        <p class="lead-26 pi-text-base">統一平台統一納管</p>
		<p class="lead-14">
			在數據中心的生命週期中，由於預算、儲存設備自身能力、臨時性的資源和儲存需求等因素，使得數據中心不可避免地需要混搭來自不同供應商的各種類型（SAN、NAS）和性能參數的儲存設備。隨著越來越多不同類型的儲存設備需要進行混搭使用，整個數據中心的儲存資源管理也將變得越來越複雜。每種儲存設備都提供了獨立的管理界面，而管理員即使是做簡單的設備監控這樣的任務，也不得不在每個設備的管理界面中頻繁切換。
		</p>
		<p class="lead-14">
			VirtualStor™ 提供了一個統一的儲存管理平台，用戶無需再糾結選擇何種儲存類型。隨著VirtualStor™ 節點數的增加，每一個儲存節點都將無縫地成為單一的、巨大的、去中心化的儲存資源池的一部分,而該儲存資源池可按用戶需求被劃分為各種類型的儲存。VirtualStor™ 通過將硬體儲存設備抽象成一個邏輯層來實現這一功能。VirtualStor™ 是一個基於對象儲存的解決方案，因此所有的儲存設備（SSD，機械硬碟）都被抽象成為對象儲存設備（OSD）。管理員可以將多塊SSD固態硬碟或機械硬碟整合成一個OSD,再由所有的OSD一起組建成一個巨大的儲存資源池(SRP)。管理員還可以通過劃分OSD組，進一步將該儲存資源池劃分為多個小的儲存資源池以滿足基礎架構的需要。不同的儲存資源池之間可以共用OSD而不會引起數據混亂和衝突。內容尋址儲存（CAS）功能可通過在儲存資源池中開啟Amazon S3或OpenStack Swift協議來支持。VirtualStor™ 可在同一集群中支持同時創建網絡附加儲存（NAS）以及儲存區域網絡（SAN）兩種類型的儲存。這些類型的儲存均支持應用最廣泛的儲存訪問協議：NAS（支持NFS、CIFS），SAN（支持iSCIS、FC）。
		</p>
		<p class="lead-14">
			由於所有的儲存類型均統一由VirtualStor™ 控制台管理，因此管理依舊非常簡單。VirtualStor™ 提供了統一的去中心化的平台管理NAS,SAN,CAS等儲存類型。只要VirtualStor™ 儲存集群中任意一台節點可用，管理控制台即可被訪問。管理平台提供了一個圖形化界面直觀地管理和監控所有的儲存資源。管理員也可設置警告策略實現主動監控機制。 
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			VirtualStor™ 管理平台為管理員提供了儲存資源的完全控制能力。管理員可為每個虛擬儲存器直接配置其儲存類型（NAS, SAN, CAS），容量，QoS(IOPS，儲存訪問頻寬)，數據可訪問性（針對應用程序）以及數據服務能力（壓縮，去重，加密）。VirtualStor™ 同時提供了一組開放的restful的管理API使得管理員可直接將其無縫融入到企業自身的管理平台和基礎架構中。
		</p>

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="efficient">

        <p class="lead-26 pi-text-base">更少資源，更大能力</p>
		<p class="lead-14">
			隨著公司的成長，其IT基礎架構也會持續擴展。這需要大量的時間、人力、資金方面的投入，從而也引入了儲存容量規劃的問題，進一步引發儲存空間過量購置的問題。VirtualStor™ 通過管理員可控的數據服務技術以及自動優化的技術完美解決了儲存容量規劃和過量購置的問題。
		</p>
		<p class="lead-14">
			管理員可通過配置多種數據服務在實際儲存空間中攫取更大的擴展空間，包括數據壓縮，數據去重以及糾刪碼等技術。數據壓縮採用無損壓縮算法，減少數據儲存所需的空間。數據去重技術避免重複數據儲存，極大減少了空間使用量。而糾刪碼是一種極為高效的數據保護機制。VirtualStor™ 將數據文件分割成多個數據塊(例如文件被分割成A,B,C,和D四個數據塊)，將這四個數據塊分佈儲存於集群的多個節點中。當啟用糾刪碼時，針對上述的四個數據塊，將生成一個校驗碼塊（A+B+C+D=校驗碼，這裡的「+」是一種矩陣邏輯運算）。當任何一個數據塊遭到破壞時，VirtualStor™ 依舊可根據校驗碼塊將遭破壞的數據恢復回來。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			VirtualStor™ 可通過多種方式自動優化儲存基礎架構。首先，VirtualStor™ 默認支持自動精簡配置，做到資源的按需分配。其次，儲存訪問可在集群的各節點間負載均衡，避免出現某個節點訪問過載情況，極大延長了儲存設備的使用壽命。最後，VirtualStor™ 適配了網絡資源以充分利用可用的基礎設施。如果數據中心為儲存配置有SSD固態硬碟，VirtualStor™ 可利用作為熱點數據緩存來提升熱點數據的訪問性能，而對於大容量需求的冷數據則使用機械硬碟進行儲存。
		</p>

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="resilience">

        <p class="lead-26 pi-text-base">健壯性及高可用性</p>
		<p class="lead-14">
			無論一個解決方案運轉得再好，其健壯性和高可用性都是非常關鍵的指標。數據和業務的持續可用對任何公司而言都非常重要。VirtualStor™ 從數據訪問高可用性和數據安全方面入手，確保數據，應用以及業務的可持續性。
		</p>
		<p class="lead-14">
			對於任何業務而言，數據高可用性都非常重要。VirtualStor™ 的數據高可用功能包括數據實時副本，糾刪碼，自我修復以及軟RAID等。VirtualStor™ 實時將數據分割成小數據塊，並將這些數據塊均勻地分發到VirtualStor™ 的對象儲存設備(OSD)中。管理員可以配置VirtualStor™ 為數據塊生成10份副本數據。每多一份副本數據，均可提升一級數據的高可用性。這一機制意味著對於任何一個數據塊都沒有單點故障存在。啟用數據副本帶來的額外好處是可以提升業務應用讀取數據的性能。
		</p>
		<p class="lead-14">
			當磁碟利用率成為儲存管理的重點時，糾刪碼為管理員提供了另一種數據複製技術。當VirtualStor™ 將數據分割成小的數據塊並分佈儲存於集群的各個OSD中時，糾刪碼技術自動為這些數據塊生成校驗塊。當這些數據塊中的任何一塊或若干塊丟失或損毀，VirtualStor™ 借助於校驗塊依舊可將丟失或損毀的數據塊恢復回來。
		</p>
		<p class="lead-14">
			VirtualStor™ 智能監控著儲存集群中的每個數據塊，以實現自我修復以及快照備份功能。首先，當集群中某台設備出現故障，亦或數據塊丟失(這可能發生在管理員將某個儲存設備從集群中移除的場景中)或損毀，VirtualStor™ 會立即從其他的數據塊副本中複製出完整的數據塊，並將其存放於新的安全的位置。其次，數據塊切分使得VirtualStor™ 能夠更高效地創建快照，從而更好地支持數據備份和恢復功能。
		</p>
		<p class="lead-14">
			VirtualStor™將數據均勻儲存於各個儲存設備和節點上。當VirtualStor™ 管理舊有儲存設備時，對舊有儲存設備中的數據訪問將會自動轉移到VirtualStor™ 儲存集群中來。VirtualStor™ 自動數據遷移功能使得管理員更加自由靈活地汰換儲存設備而無須擔心數據損壞或業務中斷。
		</p>
		<p class="lead-14">
			VirtualStor™ 採用了DNS輪詢技術以及IP接管技術來確保儲存系統的高可用性。DNS輪詢採用一組IP地址指向儲存集群域名來平衡儲存訪問負載。無論何種原因導致節點故障，DNS輪詢將採用IP地址組中可訪問的地址，繼續提供儲存服務。在多節點 (3節點、4節點或更多節點) 集群中，當任何一個節點出現故障時，剩下的健康節點均可通過IP接管機制，將故障節點的IP地址以及業務負載無縫接管過去，確保了業務訪問的高可用性。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			VirtualStor™ 可採用IntelRAES-NI加密技術或軟件加密技術保護儲存於S3儲存資源池中的數據。用戶可根據數據和應用的安全需求來開關加密功能。
		</p>

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="flexibility">

        <p class="lead-26 pi-text-base">高適配型儲存</p>
		<p class="lead-14">
			數據中心管理員一直以來都為滿足業務需求而面臨著的各種挑戰。管理員不得不基於數據中心的基礎架構以及其可提供的解決方案，來不斷嘗試調整以迎合用戶需求。而這正是VirtualStor™ 的優勢和強項。
		</p>
		<p class="lead-14">
			VirtualStor™ 極為靈活，可按用戶需求進行定制化配置。無論是儲存類型(NAS, SAN, CAS)，容量，性能（IOPS或儲存訪問頻寬）還是數據安全策略等客戶關注和需要權衡的主要指標，VirtualStor™ Sacler 都可提供彈性的解決方案。
		</p>
		<p class="lead-14">
			對於容量配置，VirtualStor™ 不僅可以通過橫向擴展節點，做到無縫擴容，同時管理員也可以通過啟動數據服務（數據壓縮，數據去重）以及數據保護機制（糾刪碼，實時副本及軟RAID）來攫取更多的儲存空間。
		</p>
		<p class="lead-14">
			如果客戶更關注性能，不僅可以通過橫向擴展VirtualStor™ 節點來獲取更高的IOPS和頻寬性能，VirtualStor™ 還提供了SSD加速技術（數據緩存，順序寫入）以及全局緩存技術進一步提升IOPS。VirtualStor™ 也可通過啟用實時副本技術進一步提升儲存訪問頻寬。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			最後，VirtualStor™ 可供管理員按需配置數據安全策略。在確保安全級別的情況下，糾刪碼和軟RAID可更有效利用儲存空間，實時多副本可以支持更高的性能要求和業務負載。同時還可配置定時快照，雲端備份來進一步保護數據。
		</p>
        
        </div>

	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>