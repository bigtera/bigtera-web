<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 產品系列";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">產品系列</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首頁</a></li>
				<li>產品系列</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled"><a id="sds"></a>
	<div class="pi-section pi-padding-bottom-40">
	    <div class="pi-row pi-padding-top-30">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:50px">
	      	<img src="<?php echo $url; ?>img/product/1U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ SDS Controller</p>
		<p class="lead-14">
			整合、優化並簡化軟體定義儲存基礎架構
		</p>
		<p class="lead-14">
			VirtualStor™ SDS Controller 在煥活和提升儲存基礎架構性能的同時顯著提升投資回報率。SDS Controller可將既有的NAS, DAS以及SAN儲存整合、匯聚成一個易於管理、可彈性配置、容量巨大的儲存設備。管理員可方便地從該儲存設備中創建虛擬儲存池（SAN，NAS以及對象）並部署諸多數據服務， 同時可定義虛擬儲存器的容量、性能（IOPS和帶寬），可訪問性以及可用性。
		</p>
		  </div>
	    </div>
	    <a id="scaler"></a>
		<div class="pi-row pi-padding-top-80">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:20px">
	      	<img src="<?php echo $url; ?>img/product/2U4U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ Scaler</p>
		<p class="lead-14">
			高性能統一橫向擴展儲存
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			VirtualStor™ Scaler 為用戶提供了一種經濟高效的橫向擴展儲存解決方案，允許用戶隨業務增長逐步支付儲存設備的費用。Scaler的橫向擴展架構在提供高可用及數據安全的儲存空間的同時，也提供了彈性配置儲存類型（SAN, NAS和對象儲存）、性能（IOPS以及帶寬）以及效率的能力。
		</p>
		  </div>
	    </div>
	    <a id="converger"></a>
        <div class="pi-row pi-padding-top-50">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:30px">
	      	<img src="<?php echo $url; ?>img/product/2U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ Converger</p>
		<p class="lead-14">
			跨平台的統一融合型儲存
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			VirtualStor™ Converger 為用戶提供了一款超融合、彈性配置以及可橫向擴展的儲存解決方案。Converger可在多種虛擬化平台（KVM, VMWare, Hyper-V）上進行部署，融合了高性能和簡化儲存基礎架構管理的特性。
		</p>
		  </div>
	    </div>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>