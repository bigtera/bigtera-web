<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 产品系列";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">产品系列</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首页</a></li>
				<li>产品系列</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled"><a id="sds"></a>
	<div class="pi-section pi-padding-bottom-40">
	    <div class="pi-row pi-padding-top-30">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:50px">
	      	<img src="<?php echo $url; ?>img/product/1U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ SDS Controller</p>
		<p class="lead-14">
			整合、优化并简化软件定义存储基础架构
		</p>
		<p class="lead-14">
			VirtualStor™ SDS Controller 在焕活和提升存储基础架构性能的同时显著提升投资回报率。SDS Controller可将既有的NAS, DAS以及SAN存储整合、汇聚成一个易于管理、可弹性配置、容量巨大的存储设备。管理员可方便地从该存储设备中创建虚拟存储池（SAN，NAS以及对象）并部署诸多数据服务， 同时可定义虚拟存储器的容量、性能（IOPS和带宽），可访问性以及可用性。
		</p>
                <p class="lead-14">
                        <a href="http://www.bigtera.com/cn/docs/Bigtera%20VirtualStor%20Controller-Datasheet_cn_z.pdf">下载产品简介</a>
                </p>
		  </div>
	    </div>
	    <a id="scaler"></a>
		<div class="pi-row pi-padding-top-80">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:20px">
	      	<img src="<?php echo $url; ?>img/product/2U4U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ Scaler</p>
		<p class="lead-14">
			高性能统一横向扩展存储
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			VirtualStor TM Scaler 为用户提供了一种经济高效的横向扩展存储解决方案，允许用户随业务增长逐步支付存储设备的费用。Scaler的横向扩展架构在提供高可用及数据安全的存储空间的同时，也提供了弹性配置存储类型（SAN, NAS和对象存储）、性能（IOPS以及带宽）以及效率的能力。
		</p>
                <p class="lead-14">
                        <a href="http://www.bigtera.com/cn/docs/Bigtera%20VirtualStor%20Scaler-Datasheet_cn_z.pdf">下载产品简介</a>
                </p>

		  </div>
	    </div>
	    <a id="converger"></a>
        <div class="pi-row pi-padding-top-50">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:30px">
	      	<img src="<?php echo $url; ?>img/product/2U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ Converger</p>
		<p class="lead-14">
			跨平台的统一融合型存储
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			VirtualStor TM Converger为用户提供了一款超融合、弹性配置以及可横向扩展的存储解决方案。Converger可在多种虚拟化平台（KVM, VMWare, Hyper-V）上进行部署，融合了高性能和简化存储基础架构管理的特性。
		</p>
                <p class="lead-14">
                        <a href="http://www.bigtera.com/cn/docs/Bigtera%20VirtualStor%20Converger-Datasheet_cn_z.pdf">下载产品简介</a>
                </p>

		  </div>
	    </div>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>
