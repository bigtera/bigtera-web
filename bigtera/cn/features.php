<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 功能";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">功能</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首页</a></li>
				<li><a href="<?php echo $url; ?>product.php">产品</a></li>
				<li>功能</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->
<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-50">
        
        <!-- Tabs navigation -->
		<ul class="pi-tabs-navigation pi-responsive-sm pi-tabs-ac">
			<li class="pi-active"><a href="#features">功能列表</a></li>
			<li><a href="#architecture">产品架构</a></li>
			<li><a href="#performance">性能参数</a></li>
			<li><a href="#resilience">系統高可用性</a></li>
			<li><a href="#protection">数据安全策略</a></li>
		</ul>
		<!-- End tabs navigation -->

        <!-- Tabs content -->
		<div class="pi-tabs-content pi-tabs-content-shadow">
			
		<!-- Tabs content item -->
		<div class="pi-tab-pane pi-active" id="features">

        <!-- Table 1 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">功能列表：所有功能</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能点</th>
						<th>描述</th>
						<th style="width:21%">优势</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>
				
					<!-- Table row -->
					<tr class="rowhover">
						<td>横向扩展存储</td>
						<td>可通过增加扩展存储空间和性能</td>
						<td>高性价比，无性能 / 容量扩展瓶颈</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>支持多中存储类型</td>
						<td>支持多种标准协议的存储类型:
					      <ul>
					        <li>SAN (iSCSI, FC)</li>
					        <li>NAS (NFS, CIFS)</li>
					        <li>对象存储(Amazon S3, OpenStack Swift)</li>
				          </ul>
				        </td>
						<td>高度灵活性 / 高资源利用率</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>服务质量控制</td>
						<td>管理员可设置应用的IO性能上限</td>
						<td>更好的资源管控方式</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>基于闪存的固态盘加速技术</td>
						<td>采用SSD固态盘缓存加速技术提升IOPS</td>
						<td>高性能(IOPS)数据访问</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>实时数据副本</td>
						<td>通过实时副本机制(最大支持10副本)提升数据高可用性和访问性能</td>
						<td>数据高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>远程数据复制</td>
						<td>复制数据到异地集群</td>
						<td>数据高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>数据去重</td>
						<td>去除底层重复数据以减少存储空间消耗</td>
						<td>高效使用存储资源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>数据压缩</td>
						<td>实施无损压缩数据以减少存储空间消耗</td>
						<td>高效使用存储资源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>自动精简配置</td>
						<td>通过规划 / 分配超过实际存储容量的虚拟存储资源，实现存储资源优化管理功能</td>
						<td>优化存储资源使用</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>持续数据保护</td>
						<td>采用快照技术，实现分钟级的数据备份和恢复机制</td>
						<td>集群高可用和数据安全</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>数据加密技术</td>
						<td>提供实时数据加密/解密功能(采用Intel AES-NI算法或256位秘钥软件加密算法)</td>
						<td>数据安全策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>纠删码</td>
						<td>提供节点间RAID的数据保护机制</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>存储分层</td>
						<td>支持将不同性能的存储资源分层，提供给不同性能需求的业务应用使用</td>
						<td>业务按需获取存储资源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>数据自动平衡</td>
						<td>数据存储自动平衡到存储集群各节点中</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>负载均衡</td>
						<td>数据访问请求平衡到存储集群各节点中</td>
						<td>集群性能优化</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>自我修复功能</td>
						<td>支持RAID技术，实时副本技术以及纠删码技术修复丢失或遭破坏的数据</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>云端存储与备份功能</td>
						<td>支持数据备份至Amazon S3(或类Amazon S3云存储)中</td>
						<td>数据保护策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>软RAID</td>
						<td>在节点内提供RAID支持</td>
						<td>数据保护策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>即插即用</td>
						<td>存储节点无需预先配置即可加入既有集群，实现不中断任何业务情况下的集群无缝扩容</td>
						<td>数据保护策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>IP接管技术</td>
						<td>当存储节点故障时，实现业务无缝接管和迁移</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>数据就近存储</td>
						<td>数据被存放于更接近业务应用程度端，提升性能</td>
						<td>高效使用存储资源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>数据迁移</td>
						<td>随应用程序的使用，被纳管的旧存储设备上的数据自动迁移到VitualStor存储集群中</td>
						<td>高效使用存储资源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>去中心化架构</td>
						<td>3节点以上(包括3节点)集群部署规模，所有节点存储资源被抽象，整合为单一的，巨大的存储资源池。任何节点故障其业务将自动被其他节点接管，确保存储服务可持续性</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>开放的管理API</td>
						<td>可通过管理API同客户既有IT设备管理平台融合</td>
						<td>易于管理</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>监控和告警</td>
						<td>自动监控存储设备和资源使用状况，自动报警机制</td>
						<td>易于管理</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>系统高可用</td>
						<td>当集群中某台设备节点故障时，剩余节点自动接管并均衡故障节点负载，消除单点故障</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>VAAI支持</td>
						<td>支持VMWare vSphere 存储API——VAAI直接使用存储硬件资源</td>
						<td>提升VMWare I/O性能</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>全局内存缓存技术</td>
						<td>使用节点空闲内存构建全局内存缓存池，加速数据访问</td>
						<td>高性能(IOPS)数据访问及集群高可用</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Amazon S3资源池</td>
						<td>为Amazon S3存储创建资源池</td>
						<td>集群弹性管理机制</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>NAS设备整合</td>
						<td>支持NAS设备整合到存储集群中</td>
						<td>集群弹性管理机制</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>SAN卷克隆技术</td>
						<td>支持从既有快照中克隆SAN数据卷</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>OSD双活高可用模式</td>
						<td>支持两台OSD节点以主备或双活模式共同管理一个存储卷</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>多路径模式挂载外部存储</td>
						<td>通过多路径方式挂载外部SAN存储，避免单路径失效</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>缓存池</td>
						<td>可为较慢存储池创建缓存池以加速数据访问</td>
						<td>提升业务应用程序访问性能</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>SAN(FC)访问控制</td>
						<td>可配置FC SAN用户访问权限</td>
						<td>数据保护策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>集中日志管理和通知</td>
						<td>主动监控存储集群</td>
						<td>易于管理</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>集群状态可视化仪表盘</td>
						<td>所有存储节点状态和资源使用状况一览图</td>
						<td>易于管理</td>
					</tr>
					<!-- End table row -->
				
				</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="architecture">

        <!-- Table 2 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">架构</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能</th>
						<th>描述</th>
						<th style="width:21%">优势</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>
				
					<!-- Table row -->
					<tr class="rowclick link">
						<td>横向扩展存储</td>
						<td>可通过增加扩展存储空间和性能</td>
						<td>高性价比，无性能 / 容量扩展瓶颈</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">横向扩展架构</p>
		                  <p class="lead-14">
			              VirtualStorTM横向扩展架构意味着企业可以平滑的方式投资硬件设备，同时按需及时向数据中心添加存储资源。有效避免过量购置，容量规划等问题，使得成本持续可控。 
		                  </p>
		                  <p class="lead-14">
			              存储节点可无缝加入到VirtualStorTM存储集群中而不影响任何在线应用程序和业务。随着VirtualStorTM的横向扩展，不仅数据中心的容量会得到扩充，而且IO访问带宽以及IOPS性能也同步得到线性提升。这意味着存储容量，数据传输和处理能力都会随节点的横向扩展而显著提升。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>支持多中存储类型</td>
						<td>支持多种标准协议的存储类型
                          <ul>
                            <li>SAN (iSCSI, FC)</li>
                            <li>NAS (NFS, CIFS)</li>
                            <li>对象存储(Amazon S3, Openstack Swift)</li>
                          </ul>
						</td>
						<td>高度灵活性 / 高资源利用率</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">多种存储类型支持</p>
		                  <p class="lead-14">
			              VirtualStorTM可同时支持多种存储类型(SAN, NAS以及对象存储)。这是通过支持多种存储类型的标准访问协议而实现的。这种能力赋予管理员更多的资源使用的灵活性，尤其是在整合型的基础架构环境中。VirtualStorTM支持以下存储类型和协议：
			              <ul>
					        <li>SAN (iSCSI, FC)</li>
					        <li>NAS (NFS, CIFS)</li>
					        <li>对象存储 (Amazon S3, OpenStack Swift)</li>
				          </ul>
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>基于闪存的固态盘加速技术</td>
						<td>采用SSD固态盘缓存加速技术提升IOPS</td>
						<td>高性能(IOPS)数据访问</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">SSD固态盘加速</p>
		                  <p class="lead-14">
			              VirtualStorTM可采用多种方式为数据中心提供极速的访问性能。首先，通过SSD固态盘缓存技术，VirtualStorTM可加提升据访问和应用程序性能。当管理员启用SSD固态盘加速功能，VirtualStorTM将随机访问IO放到SSD固态盘中，而将顺序写入的IO操作放入机械硬盘中。结合SSD固态盘缓存技术和顺序写入技术，VirtualStorTM可以改善传统机械硬盘性能10倍以上。通过添加更多的SSD固态盘(SATA/SAS, PCIe)或扩展更多的VirtualStorTM节点管理员可获取更高的性能。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>纠删码</td>
						<td>提供节点间RAID的数据保护机制</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">纠删码</p>
		                  <p class="lead-14">
			              VirtualStorTM将数据文件分割成多个小的数据块(例如一个文件被分割成A,B,C和D四个数据块)并将其分布于存储集群的各个节点中。当启用纠删码技术时，会针对原始数据块生成相应的校验码数据块，当任何原始数据块遭损毁或删除，均可通过校验码数据块重新恢复丢失或损毁的数据块。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>数据自动平衡</td>
						<td>数据存储自动平衡到存储集群各节点中</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">数据平衡分布</p>
		                  <p class="lead-14">
			              VirtualStorTM将数据文件分割成多个小的数据块(例如一个文件被分割成A,B,C和D四个数据块)并将其分布于存储集群的各个节点中。VirtualStorTM通过高效的算法确保了数据块均匀分布，从而避免了单节点存储容量过载。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>负载均衡</td>
						<td>数据访问请求平衡到存储集群各节点中</td>
						<td>集群性能优化</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">负载均衡</p>
		                  <p class="lead-14">
			              数据访问请求被均匀分布于VirtualStorTM的各个节点上。从而集群节点数越多，整个存储集群的性能就越好。这也意味着随VirtualStorTM节点增加，IO访问带宽性能也随之线性提升。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>自我修复功能</td>
						<td>支持RAID技术，实时副本技术以及纠删码技术修复丢失或遭破坏的数据</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">自我修复能力</p>
		                  <p class="lead-14">
			              VirtualStorTM具备多种集群修复功能，而如何修复损伤取决于启用了何种数据保护机制。
		                  </p>
		                  <p class="lead-14">
			              当启用数据实时副本机制时，数据至少有两份副本存储于VirtualStorTM集群的不同节点中。当VirtualStorTM发现任何一台节点设备或是任何一个数据块损坏，自动修复程序将被启动，从另一份完整的数据块中将复制一份新的数据副本并将其保存到另一个健康的存储节点中。这一方案避免了设备的单点故障，同时也确保了关键数据的安全性。
		                  </p>
		                  <p class="lead-14">
			              纠删码采用了另一种方式确保数据安全。VirtualStorTM将数据文件分割成多个小的数据块(例如一个文件被分割成A,B,C和D四个数据块)并将其分布于存储集群的各个节点中。当启用纠删码技术时，会针对原始数据块生成相应的校验码数据块，当任何原始数据块遭损毁或删除，均可通过校验码数据块重新恢复丢失或损毁的数据块。而软RAID作为另一种数据保护机制其原理于纠删码如出一辙，只是软RAID作用范围仅限于一台节点中而已。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>系统高可用</td>
						<td>当集群中某台设备节点故障时，剩余节点自动接管并均衡故障节点负载，消除单点故障</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">高可用性</p>
		                  <p class="lead-14">
			              部署VirtualStorTM存储集群至少需要3台节点设备。每台设备均等价地提供数据访问能力的支撑。任何一台设备故障，IP接管技术均可将其负载和业务无缝迁移到其他健康的设备上，确保整个系统的高可用性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>即插即用</td>
						<td>存储节点无需预先配置即可加入既有集群，实现不中断任何业务情况下的集群无缝扩容</td>
						<td>数据保护策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">即插即用架构</p>
		                  <p class="lead-14">
			              VirtualStorTM能自动侦测到新节点加入。新节点一旦加入，其存储资源将自动无缝融合到既有的单一大块存储池中。随后VirtualStorTM开始重新平衡数据到存储池中的每个节点中。高效的数据再平衡算法确保每次新节点的加入需要平衡和迁移的数据量最小。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>开放的管理API</td>
						<td>可通过管理API同客户既有IT设备管理平台融合</td>
						<td>易于管理</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">开放的管理API</p>
		                  <p class="lead-14">
			              VirtualStorTM提供了开放的管理API，管理员可使用该组API配置和管理整个VirtualStorTM集群，同时可与现有的管理平台无缝整合。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->
				
				</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="performance">

        <!-- Table 3 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">性能</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能</th>
						<th>描述</th>
						<th style="width:21%">优势</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>横向扩展存储</td>
						<td>可通过增加扩展存储空间和性能</td>
						<td>高性价比，无性能 / 容量扩展瓶颈</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">横向扩展架构</p>
		                  <p class="lead-14">
			              VirtualStorTM横向扩展架构意味着企业可以平滑的方式投资硬件设备，同时按需及时向数据中心添加存储资源。有效避免过量购置，容量规划等问题，使得成本持续可控。
存储节点可无缝加入到VirtualStorTM存储集群中而不影响任何在线应用程序和业务。随着VirtualStorTM的横向扩展，不仅数据中心的容量会得到扩充，而且IO访问带宽以及IOPS性能也同步得到线性提升。这意味着存储容量，数据传输和处理能力都会随节点的横向扩展而显著提升。

		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>服务质量控制</td>
						<td>管理员可设置应用的IO性能上限</td>
						<td>更好的资源管控方式</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">服务质量管控(QoS)</p>
		                  <p class="lead-14">
			              管理员可使用VirtualStorTM中自带的虚拟存储器功能为不同的用户配置不同存储访问策略。策略中可指定存储容量、存储类型、QoS、可访问性以及高可用性级别等。虚拟存储器的QoS包括三个指标：IOPS、存储访问带宽以及存储访问延迟。管理员可指定虚拟存储器的访问带宽以及IOPS上限，而存储访问延时会根据应用程序的数据访问请求自动优化。同时高性能SSD闪存加速卡可有效提升IOPS，而横向扩展存储节点可同时提升IOPS以及访问带宽。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>基于闪存的固态盘加速技术</td>
						<td>采用SSD固态盘缓存加速技术提升IOPS</td>
						<td>高性能(IOPS)数据访问</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">SSD固态盘加速</p>
		                  <p class="lead-14">
			              VirtualStorTM可采用多种方式为数据中心提供极速的访问性能。首先，通过SSD固态盘缓存技术，VirtualStorTM可加提升据访问和应用程序性能。当管理员启用SSD固态盘加速功能，VirtualStorTM将随机访问IO放到SSD固态盘中，而将顺序写入的IO操作放入机械硬盘中。结合SSD固态盘缓存技术和顺序写入技术，VirtualStorTM可以改善传统机械硬盘性能10倍以上。通过添加更多的SSD固态盘(SATA/SAS, PCIe)或扩展更多的VirtualStorTM节点管理员可获取更高的性能。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>存储分层</td>
						<td>支持将不同性能的存储资源分层，提供给不同性能需求的业务应用使用</td>
						<td>业务按需获取存储资源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">存储分层</p>
		                  <p class="lead-14">
			              VirtualStorTM可以为不同的业务应用程序指定其访问的存储类型。这意味着那些需要高性能IO的应用程序和数据可以存放于闪存固态盘中，而其他应用程序所访问的数据放置于传统的机械硬盘中，而数据归档则可以放置于云存储中(Amazon S3，OpenStack Swift)。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>数据就近存储</td>
						<td>数据被存放于更接近业务应用程度端，提升性能</td>
						<td>高效使用存储资源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">数据就近存储</p>
		                  <p class="lead-14">
			              VirtualStorTM将数据文件分割成多个小的数据块。这些数据块被VirtualStorTM顺序存储于各个节点设备上。随着应用程序的访问，VirtualStorTM可自动将应用程序频繁访问的数据块移动至距离应用程序更近的位置，提供了更好的数据访问性能。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>VAAI支持</td>
						<td>支持VMWare vSphere 存储API——VAAI直接使用存储硬件资源</td>
						<td>提升VMWare I/O性能</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>全局内存缓存技术</td>
						<td>使用节点空闲内存构建全局内存缓存池，加速数据访问</td>
						<td>高性能(IOPS)数据访问及集群高可用</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>缓存池</td>
						<td>可为较慢存储池创建缓存池以加速数据访问</td>
						<td>提升业务应用程序访问性能</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="resilience">

        <!-- Table 4 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">系统高可用性</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能</th>
						<th>描述</th>
						<th style="width:21%">优势</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>负载均衡</td>
						<td>数据访问请求平衡到存储集群各节点中</td>
						<td>集群性能优化</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">负载均衡</p>
		                  <p class="lead-14">
			              数据访问请求被均匀分布于VirtualStorTM的各个节点上。从而集群节点数越多，整个存储集群的性能就越好。这也意味着随VirtualStorTM节点增加，IO访问带宽性能也随之线性提升。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>自我修复功能</td>
						<td>支持RAID技术，实时副本技术以及纠删码技术修复丢失或遭破坏的数据</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">自我修复能力</p>
		                  <p class="lead-14">
			              VirtualStorTM具备多种集群修复功能，而如何修复损伤取决于启用了何种数据保护机制。
		                  </p>
		                  <p class="lead-14">
			              当启用数据实时副本机制时，数据至少有两份副本存储于VirtualStorTM集群的不同节点中。当VirtualStorTM发现任何一台节点设备或是任何一个数据块损坏，自动修复程序将被启动，从另一份完整的数据块中将复制一份新的数据副本并将其保存到另一个健康的存储节点中。这一方案避免了设备的单点故障，同时也确保了关键数据的安全性。
		                  </p>
		                  <p class="lead-14">
			              纠删码采用了另一种方式确保数据安全。VirtualStorTM将数据文件分割成多个小的数据块(例如一个文件被分割成A,B,C和D四个数据块)并将其分布于存储集群的各个节点中。当启用纠删码技术时，会针对原始数据块生成相应的校验码数据块，当任何原始数据块遭损毁或删除，均可通过校验码数据块重新恢复丢失或损毁的数据块。而软RAID作为另一种数据保护机制其原理于纠删码如出一辙，只是软RAID作用范围仅限于一台节点中而已。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>系统高可用</td>
						<td>当集群中某台设备节点故障时，剩余节点自动接管并均衡故障节点负载，消除单点故障</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">高可用性</p>
		                  <p class="lead-14">
			              部署VirtualStorTM存储集群至少需要3台节点设备。每台设备均等价地提供数据访问能力的支撑。任何一台设备故障，IP接管技术均可将其负载和业务无缝迁移到其他健康的设备上，确保整个系统的高可用性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>即插即用</td>
						<td>存储节点无需预先配置即可加入既有集群，实现不中断任何业务情况下的集群无缝扩容</td>
						<td>数据保护策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">即插即用架构</p>
		                  <p class="lead-14">
			              VirtualStorTM能自动侦测到新节点加入。新节点一旦加入，其存储资源将自动无缝融合到既有的单一大块存储池中。随后VirtualStorTM开始重新平衡数据到存储池中的每个节点中。高效的数据再平衡算法确保每次新节点的加入需要平衡和迁移的数据量最小。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>IP接管技术</td>
						<td>当存储节点故障时，实现业务无缝接管和迁移</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">IP接管技術</p>
		                  <p class="lead-14">
			              部署VirtualStorTM存储集群至少需要3台节点设备。每台设备均等价地提供数据访问能力的支撑。任何一台设备故障，IP接管技术均可将其负载和业务无缝迁移到其他健康的设备上，确保整个系统的高可用性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>去中心化架构</td>
						<td>3节点以上(包括3节点)集群部署规模，所有节点存储资源被抽象，整合为单一的，巨大的存储资源池。任何节点故障其业务将自动被其他节点接管，确保存储服务可持续性</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">去中心化管理平台</p>
		                  <p class="lead-14">
			              由于VirtualStorTM提供了一个统一的管理平台管理不同类型的存储资源（包括SAN, NAS以及CAS），因而保留了其一贯的简单性管理风格。只要VirtualStorTM集群中有任意一台节点可用，管理平台即可访问。管理平台提供了一个可视化仪表盘全局监控和管理存储设备以及存储资源的事情情况。管理员亦可设置告警策略实现对存储资源和集群的主动式监控。
		                  </p>
		                  <p class="lead-14">
			              VirtualStorTM管理平台赋予管理员全权掌控存储资源的能力。管理员可直观地为每个虚拟存储器配置存储类型(NAS, SAN, CAS)，容量，QoS(IOPS以及存储访问带宽)，数据高可用级别(针对业务数据)以及数据服务(数据压缩，数据去重，数据加密)等。VirtualStorTM同时也提供开放的restful管理API，使得管理员可将其无缝整合到既有的IT管理平台中。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>SAN卷克隆技术</td>
						<td>支持从既有快照中克隆SAN数据卷</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>OSD双活高可用模式</td>
						<td>支持两台OSD节点以主备或双活模式共同管理一个存储卷</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>多路径模式挂载外部存储</td>
						<td>通过多路径方式挂载外部SAN存储，避免单路径失效</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="protection">

        <!-- Table 5 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-text-base">数据保护策略</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">功能</th>
						<th>描述</th>
						<th style="width:21%">优势</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>实时数据副本</td>
						<td>通过实时副本机制(最大支持10副本)提升数据高可用性和访问性能</td>
						<td>数据高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">数据实时副本</p>
		                  <p class="lead-14">
			              VirtualStorTM将数据文件分割成小的数据块并将这些数据块分发到存储集群的各个节点上进行管理，数据块至少留存两份副本，且数据块副本被保存于不同的物理节点中已提供数据高可用能力。管理员可按需为数据块创建最多10份副本，而副本数越多，数据块的安全级别则越高。副本数可基于业务应用或虚拟存储器进行配置。更多的副本数亦可进一步提升访问性能，因为VirtualStorTM的智能监控体系明确知道哪些数据块正在被使用，而当新业务请求到达是，将其定向到那些尚未有业务或负载较轻的数据块副本上。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>远程数据复制</td>
						<td>复制数据到异地集群</td>
						<td>数据高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">远程数据复制</p>
		                  <p class="lead-14">
			              管理员可配置异步复制集群中的数据到异地集群，这是异地容灾的典型方案。例如，公司总部可能希望数据复制到分部进行备份，或是多个分部希望将数据汇聚到公司总部进行保存等。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>持续数据保护</td>
						<td>采用快照技术，实现分钟级的数据备份和恢复机制</td>
						<td>集群高可用和数据安全</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">持续数据保护</p>
		                  <p class="lead-14">
			              VirtualStorTM会持续监控存储于集群中的数据。管理员可配置粒度达分钟级的周期性快照策略。这使得管理员可以随时进行数据的备份和恢复。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>数据加密技术</td>
						<td>提供实时数据加密/解密功能(采用Intel AES-NI算法或256位秘钥软件加密算法)</td>
						<td>数据安全策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">数据加密技术</p>
		                  <p class="lead-14">
			              VirtualStorTM对于S3存储资源池中的数据可提供Intel AES-NI加密技术或长达256位秘钥的加密算法以确保数据安全性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>纠删码</td>
						<td>提供节点间RAID的数据保护机制</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">糾刪碼</p>
		                  <p class="lead-14">
			              VirtualStorTM将数据文件分割成多个小的数据块(例如一个文件被分割成A,B,C和D四个数据块)并将其分布于存储集群的各个节点中。当启用纠删码技术时，会针对原始数据块生成相应的校验码数据块，当任何原始数据块遭损毁或删除，均可通过校验码数据块重新恢复丢失或损毁的数据块。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>数据自动平衡</td>
						<td>数据存储自动平衡到存储集群各节点中</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">数据平衡分布</p>
		                  <p class="lead-14">
			              VirtualStorTM将数据文件分割成多个小的数据块(例如一个文件被分割成A,B,C和D四个数据块)并将其分布于存储集群的各个节点中。VirtualStorTM通过高效的算法确保了数据块均匀分布，从而避免了单节点存储容量过载。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>自我修复功能</td>
						<td>支持RAID技术，实时副本技术以及纠删码技术修复丢失或遭破坏的数据</td>
						<td>集群高可用性</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">自我修复能力</p>
		                  <p class="lead-14">
			              VirtualStorTM具备多种集群修复功能，而如何修复损伤取决于启用了何种数据保护机制。
		                  </p>
		                  <p class="lead-14">
			              当启用数据实时副本机制时，数据至少有两份副本存储于VirtualStorTM集群的不同节点中。当VirtualStorTM发现任何一台节点设备或是任何一个数据块损坏，自动修复程序将被启动，从另一份完整的数据块中将复制一份新的数据副本并将其保存到另一个健康的存储节点中。这一方案避免了设备的单点故障，同时也确保了关键数据的安全性。
		                  </p>
		                  <p class="lead-14">
			              纠删码采用了另一种方式确保数据安全。VirtualStorTM将数据文件分割成多个小的数据块(例如一个文件被分割成A,B,C和D四个数据块)并将其分布于存储集群的各个节点中。当启用纠删码技术时，会针对原始数据块生成相应的校验码数据块，当任何原始数据块遭损毁或删除，均可通过校验码数据块重新恢复丢失或损毁的数据块。而软RAID作为另一种数据保护机制其原理于纠删码如出一辙，只是软RAID作用范围仅限于一台节点中而已。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>云端备份与恢复功能</td>支持数据备份至Amazon S3(或类Amazon S3云存储)中</td>
						<td>数据保护策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">云端备份与恢复功能</p>
		                  <p class="lead-14">
			              云存储为业务提供了最高性价比的存储服务（就容量而言）。VirtualStorTM允许管理员将数据备份到云端或从云端恢复到本地集群中来。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>数据迁移</td>
						<td>随应用程序的使用，被纳管的旧存储设备上的数据自动迁移到VitualStor存储集群中</td>
						<td>高效使用存储资源</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-text-base pi-padding-top-10">数据迁移</p>
		                  <p class="lead-14">
			              VirtualStorTM可从其管理的存储资源中创建一个无缝过渡存储池用于数据迁移。随着业务应用程序对旧有存储设备中数据的访问，数据将被无缝迁移到新的VirtualStorTM存储集群中。这一无缝迁移能力意味着陈旧的或受损的存储设备可被在线汰换而不会影响到既有的应用，确保了业务的可持续性。
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>SAN(FC)访问控制</td>
						<td>可配置FC SAN用户访问权限</td>
						<td>数据保护策略</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>