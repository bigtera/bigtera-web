<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 公司";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">公司简介</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首页</a></li>
				<li>公司</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-40">
		<p class="lead-26 pi-text-base">关于 Bigtera</p>
		<p class="lead-14">
			Bigtera是软件定义存储平台和解决方案业界领先的颠覆性的创新者。Bigtera解决方案和存储平台提供了非常高的性能以及可用性。提供几乎无限的横向扩展能力，在存储容量扩展的同时也线性提升整个存储集群的性能，同时为客户提供了按业务增长逐步扩展基础设置的能力。
		</p>
		<p class="lead-14">
			Bigtera的使命就是将企业基础架构引入到软件定义的自由世界中。因为我们发现这样一个事实：目前的企业数据中心存储基础架构难以满足大规模数据应用以及云计算的需求。目前的数据中心基础架构由于其使用传统存储的局限性，造成存储设备和业务应用往往不得不一一对应来满足企业需求。这导致在使用存储设备时，产生大量的信息孤岛，也使得传统存储在运维方面缺乏灵活性。
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			企业CIO和IT工作者们已经开始考虑拥抱软件定义存储基础架构了(SDI)，但是他们也在担心从现有基础架构迁移到软件定义模式下可能带来的一系列问题和影响。我们知道改变一个企业的基础架构不像是拨动开关那么简单，因此我们致力于帮助企业构建一个更加自由的软件定义存储基础架构，通过颠覆性的创新技术，通过以下的核心理念帮助企业从现有的IT基础架构平滑无缝地过渡到软件定义存储的基础架构中。
		</p>
		<!-- Openings -->
		<div class="pi-tabs-vertical pi-responsive-sm">
			<ul class="pi-tabs-navigation pi-tabs-navigation-transparent" id="myTabs2">
				<li class="pi-active"><a href="#consolidation"><i class="icon-layout"></i>整合</a></li>
				<li><a href="#automation"><i class="icon-arrows-ccw"></i>自动化</a></li>
				<li><a href="#optimization"><i class="icon-gauge"></i>优化</a></li>
			</ul>
			<div class="pi-tabs-content pi-tabs-content-transparent">

				<div class="pi-tab-pane pi-active" id="consolidation">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">整合</span>
					</h3>
					<p class="lead-14">
						整合存储资源是关键的第一步。我们花费了巨大的努力去整合存储资源(SAN, NAS, DAS)以及云存储服务(Amazon S3、OpenStack Swift)。一旦企业需要整合现有资源，我们会关注在企业在更新或过渡到新的存储架构过程中，如何尽可能多地保持和利用现有基础架构中的存储资源。这不仅降低了总拥有成本(TCO)、提高了投资回报率(ROI)，同时也为基础架构的转型提供足够腾挪的空间。
					</p>
				</div>


				<div class="pi-tab-pane" id="automation">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">自动化</span>
					</h3>
					<p class="lead-14">
						数据迁移，资源配置以及数据恢复都是耗时耗力的工作，企业数据中心管理员往往需要花费大量时间处理上述类似的低附加值的工作，同时还希望能够用较少的资源提供更多的服务。通过将低附加值的任务通过基础架构自动完成，从而将数据中心运维团队组织架构从被动型组织转化为更为主动以及更加面向策略的组织架构，真正实现更少的资源，更大的能力。
					</p>
				</div>

				<div class="pi-tab-pane" id="optimization">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">优化</span>
					</h3>
					<p class="lead-14">
						自适应存储系统可实时优化系统资源，并实现存储资源利用率的最大化。具备自我学习且能够对存储基础架构使用情况进行预测分析的能力，将帮助数据中心管理员预先为存储和计算需求准备资源以及调整策略，确保整个企业基础架构的平稳运行。
					</p>
				</div>

			</div>
		</div>
		<!-- End openings -->
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>