<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - 虚拟化应用";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">虚拟化应用</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首页</a></li>
				<li><a href="">解決方案</a></li>
				<li>虚拟化应用</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-text-base">构建可伸缩的，灵活的和高效的虚拟化应用基础架构</p>
		<p class="lead-14">
			大多数企业已经开始逐步将其数据中心基础设施从物理机向虚拟机迁移。这一迁移源自企业CIO们希望进一步提高基础架构的投资回报率。这一目标通过将整合物理服务器计算资源，在一台物理服务器中运行多台虚拟机，从而进一步提升计算和存储资源的利用率来达成。
		</p>
		<p class="lead-14">
			随着私有云技术，大数据应用和解决方案的推广，数据和应用程序的需求呈爆炸式增长。CIO们正在不断寻找最优性价比的方案扩展其数据中心以满足业务需求。而超聚合存储和软件定义存储渐渐成为解决这一问题的关键所在。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>整合和聚合：整合存储资源，将存储和计算资源聚合到一个资源池中，为资源分配和管理提供了很大的灵活度。而支持横向/纵向扩展计算和存储资源正是Bigtera产品的优势所在。</li>
						<li>提升用户满意度：随着计算资源和存储资源的高度聚合，管理员可指定存储访问的QoS(IOPS以及存储访问带宽)。这为关键应用和数据提供了更为弹性的资管规划能力。同时结合存储分层技术，可确保关键业务和数据的高优先级以及高性能访问，从而保障了用户的使用满意度。</li>
						<li>高效率存储：VirtualStorTM提供了一系列的数据服务使得存储资源的利用率最大化。首先是自动精简配置功能。自动精简配置可提供超出物理容量的存储资源给业务系统，存储资源将按业务所需即时分配。数据压缩和去重功能进一步提升了存储资源使用效率。压缩技术提升了存储密度而去重技术消除了底层的重复数据块从而从有限的物理空间中攫取出更大的存储能力。</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>