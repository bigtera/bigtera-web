<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - 云存储";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(../img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">云存储</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首页</a></li>
				<li><a href="">解決方案</a></li>
				<li>云存储</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-text-base">升级您的公有云和混合云解决方案</p>
		<p class="lead-14">
			成本控制是企业转向云存储和云服务的一个主要原因。因为云架构为存储和计算资源提供了最高性价比的解决方案。Bigtera在提供更安全，更高性价比的云平台解决方案的同时更能够大幅提升公有云和企业私有云的性能。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>增加投资回报率：使您的云存储进一步具备数据压缩和数据去重能力。数据压缩可进一步提升数据存储密度而数据去重在底层通过高效算法删除重复数据块，两者均可显著减少数据存储时的空间消耗，提升云存储的可用容量。</li>
						<li>	确保数据安全：数据安全是确保业务持续运行的基础架构中非常关键的部分。纠删码、数据加密技术、远程云端数据复制技术均为确保数据安全和业务的可持续性的有效手段。Bigtera产品可通过Intel AES-NI加密技术为云存储数据提供加密服务。同样Bigtera产品中内建的纠删码技术通过将数据文件分割为多个小的数据块，并将其分布存放于集群的各节点中，同时为数据块构建校验码块，确保任意数据块丢失或损坏，依旧可通过校验码块重新生成损坏或丢失的数据块。最后Bigtera产品允许将企业私有云或存储基础架构中的数据通过Amazon S3或OpenStack Swift协议备份到共有云上。实现多级数据安全机制。</li>
						<li>	高性能解决方案：数据访问效率可能对云平台的交互能力产生巨大影响，Bigtera能使您更灵活地控制数据流入、流出云平台的带宽。</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>