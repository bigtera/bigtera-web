<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - 行业方案";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">行业方案</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首页</a></li>
				<li><a href="">解決方案</a></li>
				<li>行业方案</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-text-base">流媒体</p>
		<p class="lead-14">
			流媒体服务公司的存储需求特性为：必须存储大量的媒体数据并提供足够的访问性能以供其客户通过流模式访问媒体文件。这些公司可采用Bigtera产品不断扩容来满足日益增长的存储需求。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>高性能（访问带宽）需求：数据传输性能是流媒体应用中的关键指标。Bigtera旗下所有产品均可通过横向扩展的架构线性提升存储访问带宽以及数据传输性能。</li>
						<li>横向扩展架构：随着媒体文件的日益增长，流媒体公司所需的存储资源也随之增长。Bigtera系列产品的横向扩展架构为流媒体公司提供了一种细粒度，按业务增长逐步投资存储基础架构的解决方案。</li>
						<li>可定义的QoS：客户对于流媒体公司服务有一定的服务质量要求。Bigtera系列产品均支持管理员为每个虚拟存储器指定存储访问的QoS（包括IOSP和存储访问带宽），完美契合了流媒体服务的需求。</li>
					</ul>
				</div>
			</div>
		</p>
		<p class="lead-26 pi-text-base">视频监控</p>
		<p class="lead-14">
			视频监控行业似乎和流媒体行业面临同样的存储服务需求。<br>随着NVR捕获的视频内容不断增涨，需要存储设备提供持续稳定的存储访问带宽和数据传输性能。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>横向扩展架构：随着视频监控文件的持续增长，Bigtera系列产品的横向扩展架构为视频监控服务提供了一种细粒度，按业务增长逐步投资存储基础架构的解决方案。</li>
						<li>高性能（访问带宽）需求：数据传输性能是存储大量NVR视频监控流的关键指标。Bigtera旗下所有产品均可通过横向扩展的架构线性提升存储访问带宽以及数据传输性能。</li>
						<li>可定义的QoS：大量NVR视频监控流并发写入对存储系统提出了一定的服务质量要求。Bigtera系列产品均支持管理员为每个虚拟存储器指定存储访问的QoS（包括IOSP和存储访问带宽），完美契合了视频监控领域的需求。</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>