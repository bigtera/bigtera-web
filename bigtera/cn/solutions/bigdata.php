<?php include_once('../config.php'); ?>
<?php
$title = "Bigtera - 大数据";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">大数据应用</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首页</a></li>
				<li><a href="">解決方案</a></li>
				<li>大数据应用</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled" style="padding-bottom:100px">
	<div class="pi-section pi-padding-bottom-30">
		<p class="lead-26 pi-text-base">Bigtera为大数据应用提供最好的基础架构</p>
		<p class="lead-14">
			业务对大数据感兴趣出于多种原因。主要原因在于通过大数据分析可为商业决策提供良好的数据支撑。而Bigtera产品可帮助公司解决在处理大数据是遇到的重大问题。
			<div class="pi-row">
				<div class="pi-col-sm-12">
					<ul class="pi-list-with-icons pi-list-icons-dot">
						<li>高IOPS需求问题：云计算应用程序需要分析和可视化大量数据的处理能力，而Bigtera支持的基于闪存SSD加速的特性可为大数据计算提供足够的性能支撑。</li>
						<li>高带宽需求问题：数据传输性能在大数据计算和存储过程中同样非常关键。所有的Bigtera产品均可通过其优异的可伸缩性特性，为大数据应用提供所需的数据传输能力。</li>
						<li>容量性价比问题：Bigtera产品的可伸缩性为大数据应用存储提供了最优性价比的解决方案。当Bigtera产品部署于企业数据中心时，管理员可在不中断、不影响现有程序运行的情况下按需逐步扩展存储基础架构。因此投资是细粒度的以及按需分配的。</li>
					</ul>
				</div>
			</div>
		</p>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>