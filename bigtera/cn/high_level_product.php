<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 高级产品特性";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-12 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">高级产品特性</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">首页</a></li>
				<li><a href="<?php echo $url; ?>product.php">产品</a></li>
				<li>高级产品特性</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->
<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-50">
        
        <!-- Tabs navigation -->
		<ul class="pi-tabs-navigation pi-responsive-sm pi-tabs-ac">
			<li class="pi-active"><a href="#scaleout">横向扩展架构</a></li>
			<li><a href="#performance">高性能</a></li>
			<li><a href="#unified">统一存储</a></li>
			<li><a href="#efficient">高效存储</a></li>
			<li><a href="#resilience">高可用性</a></li>
			<li><a href="#flexibility">高度灵活性</a></li>
		</ul>
		<!-- End tabs navigation -->

        <!-- Tabs content -->
		<div class="pi-tabs-content pi-tabs-content-shadow">
			
		<!-- Tabs content item -->
		<div class="pi-tab-pane pi-active" id="scaleout">

        <p class="lead-26 pi-text-base">随业务增长逐步投资</p>
		<p class="lead-14">
			近年来，随着企业数据的迅速膨胀，存储业务响应要求越来越快，如何找到一个有效且高效的存储解决方案已经成为业界IT管理员们共同面临的巨大挑战。针对上述的各个问题，最大的挑战莫过于数据量的暴涨湮没了存储基础架构能力。针对大量数据的爆发式增长，存储基础架构必须能够存储和提供基于如此大量数据的业务访问能力。下一个问题就是如何处理内部用户对于存储服务的快速膨胀的需求。随着IT需求的持续增长，这将是一场用不停息的战争。最终传统存储处理大规模数据所需的高昂代价将无法维持IT预算跟上业务数据不断增长的需求。因为传统存储纵向扩展方案意味着IT基础架构在建设初期就不得不为未来存储使用需求，投入大量资金采购过量存储空间和更高性能的存储机头；同时当未来业务出现变化，一旦超出初期容量及性能规划，传统存储扩展时的设备成本、数据迁移成本以及管理成本将成指数级增长。
		</p>
		<p class="lead-14">
			Bigtera的使命就是允许企业享受软件定义的自由世界。我们的使命源于我们了解目前的数据中心基础设施难以处理大数据和云计算相关的存储需求。目前数据中心的基础架构约束了存储设备以及存储资源不得不同业务系统一一对应，这导致了存储使用时产生大量的信息孤岛，这也是在使用传统存储时不够弹性的地方。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			CIO和IT从业者们已经开始拥抱软件定义存储的基础架构的概念了，但是他们还在担心如何从既有的基础架构过渡到期望的软件定义存储架构中。我们理解改变企业的存储基础架构并不像拨动开关那样简单，因此Bigtera致力于构建企业用户可自由定义的存储基础架构，而我们的团队将所有的精力和信念都投入到创建一个颠覆性的软件定义存储平台，以帮助企业用户可以无缝地将现有的IT基础架构平滑过渡到新一代软件定义的数据中心架构中。
		</p>
        
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="performance">

        <p class="lead-26 pi-text-base">加速IT基础设施</p>
		<p class="lead-14">
			数据中心不仅需要满足存储容量的需求，同时也要提供足够的电力以及存储访问带宽供业务和应用程序使用。如今很多数据中心都需要支持各种类型的高性能应用程序的负载，包括，但不仅限于，服务器虚拟化，桌面虚拟化以及对SQL数据库应用的支持。因此处理、存储以及发布数据服务已不仅仅是容量方面的问题，而是需要同时提供更高的存储服务性能方面的支撑。然而传统存储目前仅能勉强解决存储容量的问题，仍需要寻求新的技术来处理性能和容量同步增长的需求。
		</p>
		<p class="lead-14">
			VirtualStor TM 引入多种方法同时对数据中心的存储系统进行加速。首先利用SSD固态盘缓存技术，VirtualStor TM大幅提升负载处理能力以及应用程序访问性能。当管理员启用SSD固态盘加速技术，VirtualStor TM 先将随机IO访问数据到SSD固态盘缓存中，进而整合其中的随机IO访问请求成为连续IO访问，并将其顺序写入底层机械磁盘中。结合SSD固态盘缓存和顺序写入技术，VirtualStor TM可提升传统机械磁盘存储性能达10倍以上。管理员更可增加更多的SSD(SATA/SAS, PCIe接口均可支持)或横向扩展VirtualStor TM Scaler节点来进一步提升性能。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			随着VirtualStorTM Scaler的横向扩展，整个系统的IO访问带宽以及IOPS性能均可得到显著提升。IOPS是指每秒可处理的IO请求数而IO访问带宽则反映了数据读写的速度。VirtualStorTM 集群中节点数越多，其处理和响应业务的速度就越快。VirtualStor TM 集群（VirtualStorTM一体机）甚至可以通过执行集群内部事务以及利用集群中各节点的空闲内存构建全局内存缓存池，提升更高的存储性能。管理员亦可为某些较慢的存储介质指定缓存池来加速热点数据访问。VirtualStor TM分布式文件存储架构配合实时数据副本机制，同样也能够提升数据读取的访问性能，因为VirtualStor TM系统可根据存储访问请求智能定位到最近可访问的数据块副本上。
		</p>

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="unified">

        <p class="lead-26 pi-text-base">统一平台统一纳管</p>
		<p class="lead-14">
			在数据中心的生命周期中，由于预算、存储设备自身能力、临时性的资源和存储需求等因素，使得数据中心不可避免地需要混搭来自不同供应商的各种类型（SAN、NAS）和性能参数的存储设备。随着越来越多不同类型的存储设备需要进行混搭使用，整个数据中心的存储资源管理也将变得越来越复杂。每种存储设备都提供了独立的管理界面，而管理员即使是做简单的设备监控这样的任务，也不得不在每个设备的管理界面中频繁切换。
		</p>
		<p class="lead-14">
			VirtualStor TM 提供了一个统一的存储管理平台，用户无需再纠结选择何种存储类型。随着VirtualStor TM节点数的增加，每一个存储节点都将无缝地成为单一的、巨大的、去中心化的存储资源池的一部分,而该存储资源池可按用户需求被划分为各种类型的存储。VirtualStor TM通过将硬件存储设备抽象成一个逻辑层来实现这一功能。VirtualStor TM是一个基于对象存储的解决方案，因此所有的存储设备（SSD，机械磁盘）都被抽象成为对象存储设备（OSD）。管理员可以将多块SSD固态盘或机械磁盘整合成一个OSD,再由所有的OSD一起组建成一个巨大的存储资源池(SRP)。管理员还可以通过划分OSD组，进一步将该存储资源池划分为多个小的存储资源池以满足基础架构的需要。不同的存储资源池之间可以共用OSD而不会引起数据混乱和冲突。内容寻址存储（CAS）功能可通过在存储资源池中开启Amazon S3或OpenStack Swift协议来支持。VirtuaStor TM可在同一集群中支持同时创建网络附加存储（NAS）以及存储区域网络（SAN）两种类型的存储。这些类型的存储均支持应用最广泛的存储访问协议：NAS（支持NFS、CIFS），SAN（支持iSCIS、FC）。
		</p>
		<p class="lead-14">
			由于所有的存储类型均统一由VirtualStor TM控制台管理，因此管理依旧非常简单。VirtualStor TM提供了统一的去中心化的平台管理NAS,SAN,CAS等存储类型。只要VritualStor TM存储集群中任意一台节点可用，管理控制台即可被访问。管理平台提供了一个图形化界面直观地管理和监控所有的存储资源。管理员也可设置告警策略实现主动监控机制。 
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			VritualStor TM管理平台为管理员提供了存储资源的完全控制能力。管理员可为每个虚拟存储器直接配置其存储类型（NAS, SAN, CAS），容量，QoS(IOPS，存储访问带宽)，数据可访问性（针对应用程序）以及数据服务能力（压缩，去重，加密）。VirtualStor TM同时提供了一组开放的restful的管理API使得管理员可直接将其无缝融入到企业自身的管理平台和基础架构中。
		</p>

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="efficient">

        <p class="lead-26 pi-text-base">更少资源，更大能力</p>
		<p class="lead-14">
			随着公司的成长，其IT基础架构也会持续扩展。这需要大量的时间、人力、资金方面的投入，从而也引入了存储容量规划的问题，进一步引发存储空间过量购置的问题。VritaulStorTM通过管理员可控的数据服务技术以及自动优化的技术完美解决了存储容量规划和过量购置的问题。
		</p>
		<p class="lead-14">
			管理员可通过配置多种数据服务在实际存储空间中攫取更大的扩展空间，包括数据压缩，数据去重以及纠删码等技术。数据压缩采用无损压缩算法，减少数据存储所需的空间。数据去重技术避免重复数据存储，极大减少了空间使用量。而纠删码是一种极为高效的数据保护机制。VritualStorTM将数据文件分割成多个数据块(例如文件被分割成A,B,C,和D四个数据块)，将这四个数据块分布存储于集群的多个节点中。当启用纠删码时，针对上述的四个数据块，将生成一个校验码块（A+B+C+D=校验码，这里的“+”是一种矩阵逻辑运算）。当任何一个数据块遭到破坏时，VirtualStorTM依旧可根据校验码块将遭破坏的数据恢复回来。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			VirtualStorTM可通过多种方式自动优化存储基础架构。首先，VirtualStorTM默认支持自动精简配置，做到资源的按需分配。其次，存储访问可在集群的各节点间负载均衡，避免出现某个节点访问过载情况，极大延长了存储设备的使用寿命。最后，VirtualStorTM适配了网络资源以充分利用可用的基础设施。如果数据中心为存储配置有SSD固态盘，VirtualStorTM可利用作为热点数据缓存来提升热点数据的访问性能，而对于大容量需求的冷数据则使用机械硬盘进行存储。
		</p>

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="resilience">

        <p class="lead-26 pi-text-base">健壮性及高可用性</p>
		<p class="lead-14">
			无论一个解决方案运转得再好，其健壮性和高可用性都是非常关键的指标。数据和业务的持续可用对任何公司而言都非常重要。VirtualStorTM从数据访问高可用性和数据安全方面入手，确保数据，应用以及业务的可持续性。
		</p>
		<p class="lead-14">
			对于任何业务而言，数据高可用性都非常重要。VirtualStorTM的数据高可用功能包括数据实时副本，纠删码，自我修复以及软RAID等。VirtualStorTM实时将数据分割成小数据块，并将这些数据块均匀地分发到VirtualStorTM的对象存储设备(OSD)中。管理员可以配置VirtualStorTM为数据块生成10份副本数据。每多一份副本数据，均可提升一级数据的高可用性。这一机制意味着对于任何一个数据块都没有单点故障存在。启用数据副本带来的额外好处是可以提升业务应用读取数据的性能。
		</p>
		<p class="lead-14">
			当磁盘利用率成为存储管理的重点时，纠删码为管理员提供了另一种数据复制技术。当VirtualStorTM将数据分割成小的数据块并分布存储于集群的各个OSD中时，纠删码技术自动为这些数据块生成校验块。当这些数据块中的任何一块或若干块丢失或损毁，VirtualStorTM借助于校验块依旧可将丢失或损毁的数据块恢复回来。
		</p>
		<p class="lead-14">
			VirtualStorTM智能监控着存储集群中的每个数据块，以实现自我修复以及快照备份功能。首先，当集群中某台设备出现故障，亦或数据块丢失(这可能发生在管理员将某个存储设备从集群中移除的场景中)或损毁，VirtualStorTM会立即从其他的数据块副本中复制出完整的数据块，并将其存放于新的安全的位置。其次，数据块切分使得VirtualStorTM能够更高效地创建快照，从而更好地支持数据备份和恢复功能。
		</p>
		<p class="lead-14">
			VirtualStorTM将数据均匀存储于各个存储设备和节点上。当VirtualStorTM管理旧有存储设备时，对旧有存储设备中的数据访问将会自动转移到VirtualStorTM存储集群中来。VirtualStorTM自动数据迁移功能使得管理员更加自由灵活地汰换存储设备而无须担心数据损坏或业务中断。
		</p>
		<p class="lead-14">
			VirtualStorTM采用了DNS轮询技术以及IP接管技术来确保存储系统的高可用性。DNS轮询采用一组IP地址指向存储集群域名来平衡存储访问负载。无论何种原因导致节点故障，DNS轮询将采用IP地址组中可访问的地址，继续提供存储服务。在多节点 (3节点、4节点或更多节点) 集群中，当任何一个节点出现故障时，剩下的健康节点均可通过IP接管机制，将故障节点的IP地址以及业务负载无缝接管过去，确保了业务访问的高可用性。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			VirtualStorTM可采用Intel®AES-NI加密技术或软件加密技术保护存储于S3存储资源池中的数据。用户可根据数据和应用的安全需求来开关加密功能。
		</p>

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="flexibility">

        <p class="lead-26 pi-text-base">高适配型存储</p>
		<p class="lead-14">
			数据中心管理员一直以来都为满足业务需求而面临着的各种挑战。管理员不得不基于数据中心的基础架构以及其可提供的解决方案，来不断尝试调整以迎合用户需求。而这正是VirtualStorTM的优势和强项。
		</p>
		<p class="lead-14">
			VirtualStorTM极为灵活，可按用户需求进行定制化配置。无论是存储类型(NAS, SAN, CAS)，容量，性能（IOPS或存储访问带宽）还是数据安全策略等客户关注和需要权衡的主要指标，VirtualStor TM Sacler都可提供弹性的解决方案。
		</p>
		<p class="lead-14">
			对于容量配置，VirtualStorTM不仅可以通过横向扩展节点，做到无缝扩容，同时管理员也可以通过启动数据服务（数据压缩，数据去重）以及数据保护机制（纠删码，实时副本及软RAID）来攫取更多的存储空间。
		</p>
		<p class="lead-14">
			如果客户更关注性能，不仅可以通过横向扩展VirtualStorTM节点来获取更高的IOPS和带宽性能，VirtualStor TM还提供了SSD加速技术（数据缓存，顺序写入）以及全局缓存技术进一步提升IOPS。VirtualStorTM也可通过启用实时副本技术进一步提升存储访问带宽。
		</p>
		<p class="lead-14 pi-padding-bottom-10">
			最后，VirtualStorTM可供管理员按需配置数据安全策略。在确保安全级别的情况下，纠删码和软RAID可更有效利用存储空间，实时多副本可以支持更高的性能要求和业务负载。同时还可配置定时快照，云端备份来进一步保护数据。
		</p>
        
        </div>

	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>