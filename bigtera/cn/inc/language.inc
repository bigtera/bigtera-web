<div class="pi-section-w pi-section-header-w pi-section-dark pi-section-header-sm-w">
	<div class="pi-section pi-section-header pi-clearfix">

		<div class="pi-header-block pi-pull-right pi-hidden-xs">
			<ul class="pi-lang">
                <li>
                <a class="chooselang" style="background: url(<?php echo $url; ?>img/language.png) no-repeat 0 -1px" href="javascript:void(0)">Choose Language</a>
                    <ul>
                        <li><a href='http://www.bigtera.com'>English</a></li>
                        <li><a href='http://www.bigtera.com.cn'>简体中文</a></li>
                        <li><a href='http://www.bigtera.com/tw/'>繁體中文</a></li>
                    </ul>
                </li>
		</div>

	</div>
</div>