<!DOCTYPE html>
<html>
<head>
<title><?php 
if(isset($title) && !empty($title)) { 
   echo $title; 
} 
else { 
   echo "Bigtera"; 
} ?></title>
<meta name="keywords" content="SDS, Software Defined Storage, 軟件定義存儲, SAN, NAS, DAS, tiering, IOPS, 橫向擴展存儲, 虛擬化, 資料防護, 大數據, 雲存儲, Bigtera VirtualStor, VirtualStor" />
<meta name="description" content="<?php
if(isset($metaD) && !empty($metaD)) { 
   echo $metaD; 
} 
else { 
   echo ""; 
} ?>" />
<meta property="og:image" content="http://www.bigtera.com/img/virtualstor.jpg?v=virtualstor"/>
<meta property="og:image:width" content="600" />  
<meta property="og:image:height" content="315" /> 
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $url; ?>img/icon/apple-touch-icon-57x57.png?v=hlr2015">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $url; ?>img/icon/apple-touch-icon-60x60.png?v=hlr2015">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $url; ?>img/icon/apple-touch-icon-72x72.png?v=hlr2015">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $url; ?>img/icon/apple-touch-icon-76x76.png?v=hlr2015">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $url; ?>img/icon/apple-touch-icon-114x114.png?v=hlr2015">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $url; ?>img/icon/apple-touch-icon-120x120.png?v=hlr2015">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $url; ?>img/icon/apple-touch-icon-144x144.png?v=hlr2015">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $url; ?>img/icon/apple-touch-icon-152x152.png?v=hlr2015">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $url; ?>img/icon/apple-touch-icon-180x180.png?v=hlr2015">
<link rel="icon" type="image/png" href="<?php echo $url; ?>img/icon/favicon-32x32.png?v=hlr2015" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo $url; ?>img/icon/favicon-194x194.png?v=hlr2015" sizes="194x194">
<link rel="icon" type="image/png" href="<?php echo $url; ?>img/icon/favicon-96x96.png?v=hlr2015" sizes="96x96">
<link rel="icon" type="image/png" href="<?php echo $url; ?>img/icon/android-chrome-192x192.png?v=hlr2015" sizes="192x192">
<link rel="icon" type="image/png" href="<?php echo $url; ?>img/icon/favicon-16x16.png?v=hlr2015" sizes="16x16">
<link rel="manifest" href="<?php echo $url; ?>img/icon/manifest.json?v=hlr2015">
<link rel="shortcut icon" href="<?php echo $url; ?>img/icon/favicon.ico?v=hlr2015">
<meta name="msapplication-TileColor" content="#0071ab">
<meta name="msapplication-TileImage" content="<?php echo $url; ?>img/icon/mstile-144x144.png?v=hlr2015">
<meta name="msapplication-config" content="<?php echo $url; ?>img/icon/browserconfig.xml?v=hlr2015">
<meta name="theme-color" content="#ffffff">
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/style.css?v=blr2015"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/loader.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>3dParty/rs-plugin/css/pi.settings.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/slider.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/tabs.css?v=hlr2015"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/tables.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/counters.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/galleries.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/images.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/portfolio.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/alert-boxes.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/animations.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/animate.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/tooltips.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>3dParty/colorbox/colorbox.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/google-maps.css"/>
<!--Fonts-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<!--Fonts with Icons-->
<link rel="stylesheet" href="<?php echo $url; ?>3dParty/fontello/css/fontello.css"/>
<!--[if lte IE 8]>
<script src="<?php echo $url; ?>scripts/respond.min.js"></script>
<![endif]-->
</head>

<body>

<div id="pi-all">

<!-- Header -->
<div class="pi-main-header-w">

<!-- language -->
<?php require_once(ROOT ."inc/language.inc"); ?>
<!-- End language -->

<div class="pi-header-row-sticky-w">
<!-- Header row -->
<div class="pi-section-w pi-section-header-w pi-section-white pi-section-header-lg-w pi-header-row-sticky pi-shadow-bottom">
<div class="pi-section pi-section-header pi-section-header-lg pi-clearfix">

<!-- Logo -->
<div class="pi-header-block pi-header-block-logo">
	<a href="<?php echo $url; ?>"><img src="<?php echo $url; ?>img/bigtera_logo.svg" alt=""></a>
</div>
<!-- End logo -->

<!-- Text -->
<div class="pi-header-block pi-header-block-txt pi-hidden-2xs pi-smtxt">Empowering Your Software-Defined Transformation</div>
<!-- End text -->

<?php require_once(ROOT ."inc/nav.inc"); ?>

</div>
</div>
<!-- End Header row -->
</div>

</div>
<!-- End Header -->
