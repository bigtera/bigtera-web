<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - Company";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">Company Overview</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">Home</a></li>
				<li>Company</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-40">
		<p class="lead-26 pi-weight-700 pi-text-base">About Bigtera</p>
		<p class="lead-14">
			Bigtera is a disruptive innovator of industry-leading enterprise software-defined storage solutions and platforms. Bigtera solutions and platforms are designed to perform with extraordinarily high performance and availability, deliver unlimited scale-out storage capacity that scales with performance, and provide a cost effective, pay-as-you-grow business model.
		</p>
		<p class="lead-14">
			Our mission at Bigtera is empowering enterprises with software-defined freedom. Our mission is derived from the fact that we know that current data center infrastructures have difficulty handling big data and cloud computing, in terms of storage. Current infrastructures are caught in restrictive storage silos with storage hardware typically assigned in a one to one ratio with applications. This results in a huge amount of isolated and under utilized storage. There is also a lack of agility with how traditional storage infrastructures operate.
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			CIOs and IT staff have been embracing the idea of software-defined infrastructure (SDI), but they have concerns surrounding the transition from what they have to what they want. We understand that transforming an enterprise’s infrastructure is not something that happens at the flip of a switch. We are committed to empowering enterprises with the freedom to define their storage infrastructure so our team of disruptive innovators have focused their efforts around the following core beliefs to facilitate a smooth seamless transition to the next generation of data center infrastructures:
		</p>
		<!-- Openings -->
		<div class="pi-tabs-vertical pi-responsive-sm">
			<ul class="pi-tabs-navigation pi-tabs-navigation-transparent" id="myTabs2">
				<li class="pi-active"><a href="#consolidation"><i class="icon-layout"></i>Consolidation</a></li>
				<li><a href="#automation"><i class="icon-arrows-ccw"></i>Automation</a></li>
				<li><a href="#optimization"><i class="icon-gauge"></i>Optimization</a></li>
			</ul>
			<div class="pi-tabs-content pi-tabs-content-transparent">

				<div class="pi-tab-pane pi-active" id="consolidation">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">Consolidation</span>
					</h3>
					<p class="lead-14">
						Consolidation of storage resources is a critical first step. We have spent considerable effort understanding how to make consolidation of on-premise (SAN, NAS, DAS) and cloud service (Amazon S3, OpenStack Swift) storage resources a reality. Once an enterprise’s resources are agnostically consolidated, we looked at how to keep existing storage resources a part of the infrastructure as enterprises transition to newer or replacement storage. This decreases the TCO and increases the ROI for existing resources and provides breathing room for the transformation.
					</p>
				</div>


				<div class="pi-tab-pane" id="automation">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">Automation</span>
					</h3>
					<p class="lead-14">
						Data migration from one storage node to another storage node, resource provisioning, and data recovery can take a great deal of time. Data center staff spend a great deal of time buried by low value add tasks like those mentioned above, all the while trying to do more with less. By automating many of these low value add tasks, data center staff are able to transform from a reactive organization to a more proactive and strategic organization.
					</p>
				</div>

				<div class="pi-tab-pane" id="optimization">
					<h3 class="pi-weight-300 pi-title">
						<span class="pi-text-base">Optimization</span>
					</h3>
					<p class="lead-14">
						Adaptive systems are able to optimize the utilization of resources in real time to maximize the capabilities of all existing storage resources. Self learning capabilities can also produce predictive analysis of infrastructure resources to keep data center staff well ahead of storage and compute needs.
					</p>
				</div>

			</div>
		</div>
		<!-- End openings -->
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>
