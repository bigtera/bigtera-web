<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - 404";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white">
	<div class="pi-section pi-text-center pi-padding-top-120 pi-padding-bottom-120">
	
		<p class="pi-text-dark pi-404">
			404
		</p>
		<p class="lead-30 pi-weight-700 pi-uppercase pi-margin-bottom-10">
			Page not Found!
		</p>
		<p>
			We're sorry, the page you requested cannot be found.<br>
			You can go back to
		</p>
		<p>
			<a href="<?php echo $url; ?>" class="btn pi-btn pi-btn-base pi-btn-big-paddings pi-btn-big">
				Homepage
			</a>
		</p>
		
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>