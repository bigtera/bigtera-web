<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - Features";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">Features</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">Home</a></li>
				<li><a href="<?php echo $url; ?>product.php">Products</a></li>
				<li>Features</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->
<div class="pi-section-w pi-section-white pi-slider-enabled">
	<div class="pi-section pi-padding-bottom-50">
        
        <!-- Tabs navigation -->
		<ul class="pi-tabs-navigation pi-responsive-sm pi-tabs-ac">
			<li class="pi-active"><a href="#features">Feature Matrix</a></li>
			<li><a href="#architecture">Architecture</a></li>
			<li><a href="#performance">Performance</a></li>
			<li><a href="#resilience">System Resilience</a></li>
			<li><a href="#protection">Data Protection</a></li>
		</ul>
		<!-- End tabs navigation -->

        <!-- Tabs content -->
		<div class="pi-tabs-content pi-tabs-content-shadow">
			
		<!-- Tabs content item -->
		<div class="pi-tab-pane pi-active" id="features">

        <!-- Table 1 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-weight-700 pi-text-base">Feature Matrix: All Features</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">Feature</th>
						<th>Description</th>
						<th>Benefit</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>
				
					<!-- Table row -->
					<tr class="rowhover">
						<td>Scale-out storage</td>
						<td>Expand storage capacity and performance by adding more nodes</td>
						<td>Cost effective non-disruptive capacity and performance expansion</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Storage type support</td>
						<td>Supported storage types with protocols:
					      <ul>
					        <li>SAN (iSCSI, FC)</li>
					        <li>NAS (NFS, CIFS)</li>
					        <li>object storage (Amazon S3, OpenStack Swift)</li>
				          </ul>
				        </td>
						<td>Efficient resource utilization</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Quality of Service</td>
						<td>Administrators can specify the performance ceiling for applications or workloads</td>
						<td>Resource management</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Flash-based SSD acceleration</td>
						<td>Accelerate IOPS using SSD caching technology</td>
						<td>High performance (IOPS) for applications and data delivery</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Real-time data replication</td>
						<td>Provide better data availability and performance by creating duplicate data (up to 10 copies)</td>
						<td>Configure data availability</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Remote data replication</td>
						<td>Replicate data to remote or branch locations</td>
						<td>Data consistency across the infrastructure</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Data deduplication</td>
						<td>Reduce the capacity required for storage eliminating duplicate information</td>
						<td>Efficient resource utilization</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Data compression</td>
						<td>Reduce the capacity required for storage by compressing the data on the fly before writing it to disk</td>
						<td>Efficient resource utilization</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Thin provisioning</td>
						<td>Optimize utilization of resources by virtually providing more resources than are physically available</td>
						<td>Optimize resource usage</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Continuous data protection</td>
						<td>Up to the minute efficient backups and quick recovery, using storage snapshot technology</td>
						<td>Infrastructure resilience and availability</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Data encryption</td>
						<td>Real-time data encryption/decryption technology using Intel AES-NI (when available) or by 256 bit software encryption</td>
						<td>Data security</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Erasure coding</td>
						<td>Provides network RAID-like protection technology across storage nodes</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Performance tiering</td>
						<td>Specify different storage resources for applications or workloads to match their requirements</td>
						<td>Applications and workloads get the storage and compute resources to match their requirements</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Data balancing</td>
						<td>Balance data equally across storage devices</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Load balancing</td>
						<td>Data requests and delivery are balanced equally across storage nodes</td>
						<td>Infrastructure performance</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Self-repairing</td>
						<td>Missing or damaged data blocks are repaired by using RAID, data replication, or erasure coding technologies</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Cloud backup and restore</td>
						<td>Backup data to Amazon S3 storage (or storage that mimics Amazon S3)</td>
						<td>Data protection</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Software RAID</td>
						<td>Provides RAID technology within individual storage nodes </td>
						<td>Data protection</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Plug and play architecture</td>
						<td>Storage nodes can be added without disruption to services or applications. Device configuration is not needed when a storage node is added.</td>
						<td>Data protection</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>IP takeover</td>
						<td>Seamless service take over from storage node to storage node when issues occur within the same cluster.</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Data location awareness</td>
						<td>Data is moved closer to applications for improved performance</td>
						<td>Efficient resource utilization</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Data migration</td>
						<td>As applications request data, data stored on legacy storage is automatically moved to VirtualStor.</td>
						<td>Efficient resource utilization</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Decentralized architecture</td>
						<td>VirtualStor deploys as three or more nodes across an infrastructure. As VirtualStor deploys, storage becomes a single massive storage entity. If one of the nodes encounters issues the other nodes take over, ensuring service continuity.</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Open management API</td>
						<td>Administrators can use VirtualStor APIs for further management in their infrastructure.</td>
						<td>Infrastructure agility</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Monitoring and alerts</td>
						<td>VirtualStor can monitor your storage and devices and send alerts to administrators.</td>
						<td>Ease of management</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>System high availability</td>
						<td>If a VirtualStor node encounters issues, the remaining nodes take over or balance the workload. This prevents a single point of failure.</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>VAAI support</td>
						<td>Support VMware vSphere Storage APIs – Array Integration(VAAI) to utilize storage hardware to accomplish certain tasks</td>
						<td>Improve VMware I/O performance</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Memory caching cluster</td>
						<td>Clustering unused memory as a cache for applications and workloads</td>
						<td>High performance (IOPS) for applications and data delivery and infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Amazon S3 resource pools</td>
						<td>Create Virtual Storage pools of Amazon S3 storage</td>
						<td>Infrastructure flexibility</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>NAS consolidation</td>
						<td>NAS storage is absorbed into the single massive storage entity</td>
						<td>Infrastructure flexibility</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>SAN volume clone</td>
						<td>Copy-on-write clones for SAN volumes from an existing snapshot</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>OSD high availability in Active-Active mode</td>
						<td>If an object storage device (OSD) enounters issues, workload traffic is redirected or load balanced to OSD that are working properly</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Multi-path support for external attached storage</td>
						<td>External attached SAN storage can be configured with multiple paths to prevent single path failures</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Caching pool</td>
						<td>Administrators can create cache pools for slower storage pools to increase hot data accessing performance</td>
						<td>Improved application and workload performance</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>SAN (FC) access control</td>
						<td>Administrators can define user access to SAN (FC) storage</td>
						<td>Data protection</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Central logging and notifications</td>
						<td>Administrators can proactively monitor their infrastructure</td>
						<td>Ease of management</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowhover">
						<td>Cluster status dashboard</td>
						<td>Administrators have at-a-glance access to all storage nodes</td>
						<td>Ease of management</td>
					</tr>
					<!-- End table row -->
				
				</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="architecture">

        <!-- Table 2 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-weight-700 pi-text-base">Architecture</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">Feature</th>
						<th>Description</th>
						<th>Benefit</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>
				
					<!-- Table row -->
					<tr class="rowclick link">
						<td>Scale-out storage</td>
						<td>Expand storage capacity and performance by adding more nodes</td>
						<td>Cost effective non-disruptive capacity and performance expansion</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Scale out architecture</p>
		                  <p class="lead-14">
			              The scale out architecture of VirtualStor™ means companies can use a modest investment in commodity hardware and then add storage to their data centers just in time. This eliminates over provisioning, brings resource cost under control, and eliminates capacity planning issues. 
		                  </p>
		                  <p class="lead-14">
			              Storage nodes can be added on the fly to VirtualStor™ with no disruption of service to applications or application workloads. As VirtualStor™ scales out, capacity is not the only benefit that data centers receive. Both throughput and IOPS also improve as VirtualStor™ scales out. This means storage capacity, data delivery, and raw processing power are all significantly improved.  
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Storage type support</td>
						<td>Supported storage types with protocols:
					      <ul>
					        <li>SAN (iSCSI, FC)</li>
					        <li>NAS (NFS, CIFS)</li>
					        <li>object storage (Amazon S3, OpenStack Swift)</li>
				          </ul>
				        </td>
						<td>Flexibility / resource utilization</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Storage Type Support</p>
		                  <p class="lead-14">
			              VirtualStor™ supports specifying the type of storage (SAN, NAS, object storage) used for applications and data. This is achieved by supporting various storage protocols for the associated storage types. This gives administrators a great deal of flexibility when it comes to resource utilization, especially in consolidated infrastructures. VirtualStor™ supports the following protocols:
			              <ul>
					        <li>SAN (iSCSI, FC)</li>
					        <li>NAS (NFS, CIFS)</li>
					        <li>object storage (Amazon S3, OpenStack Swift)</li>
				          </ul>
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Flash-based SSD acceleration</td>
						<td>Accelerate IOPS using SSD caching technology</td>
						<td>High performance (IOPS) for applications and data delivery</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">SSD Acceleration</p>
		                  <p class="lead-14">
			              VirtualStor™ brings blazing performance to your data center in several ways. First by leveraging SSD, VirtualStor™ improves data workload and application performance. When an administrator enables SSD acceleration, VirtualStor™ accesses randomly stored data, processes the data, then VirtualStor™ writes the data sequentially to HDD. Combining SSD caching and sequential writing, VirtualStor™ can improve traditional HDD storage performance by at least 10X. Administrators can improve performance further by adding more SSD (SATA/SAS, PCIe) or by scaling VirtualStor™ out.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Erasure coding</td>
						<td>Provides network RAID-like protection technology across storage nodes</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Erasure Coding</p>
		                  <p class="lead-14">
			              VirtualStor™ breaks data down into blocks (a file is broken into blocks A, B, C, and D) and distributes the blocks across the storage infrastructure. When erasure coding is enabled, a parity file is created for the blocks to recreate the original file. If any of the blocks are corrupt or deleted, VirtualStor™ uses the parity file to recreate the deleted or corrupt data blocks.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Data balancing</td>
						<td>Balance data equally across storage devices</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Data Balancing</p>
		                  <p class="lead-14">
			              VirtualStor™ breaks data down into blocks (a file is broken into blocks A, B, C, and D) and distributes the blocks across the storage infrastructure. VirtualStor™ distributes the data blocks evenly across the storage infrastructure. 
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Load balancing</td>
						<td>Data requests and delivery are balanced equally across storage nodes</td>
						<td>Infrastructure performance</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Load Balancing</p>
		                  <p class="lead-14">
			              Requests for data are distributed across the nodes available to VirtualStor™. The more nodes that are available, the better the throughput performance. This means that as VirtualStor™ scales out, the throughput performance increases.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Self Repairing</td>
						<td>Missing or damaged data blocks are repaired by using RAID, data replication, or erasure coding technologies</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Self Repairing</p>
		                  <p class="lead-14">
			              There are a few ways that VirtualStor™ can repair the storage infrastructure. The way a repair occurs depends on which data protection features are enabled. 
		                  </p>
		                  <p class="lead-14">
			              With data replication enabled, as VirtualStor™ breaks data up into blocks, a minimum of two copies for each data block is created and then distributed at different locations in the infrastructure. When VirtualStor™ detects a device is damaged or a data block is missing or corrupt, VirtualStor™ creates another duplicate of the block and saves the data block in a different location. This way no single device is more critical than another anywhere in the infrastructure and critical data is always available. 
		                  </p>
		                  <p class="lead-14">
			              With data replication enabled, as VirtualStor™ breaks data up into blocks, a minimum of two copies for each data block is created and then distributed at different locations in the infrastructure. When VirtualStor™ detects a device is damaged or a data block is missing or corrupt, VirtualStor™ creates another duplicate of the block and saves the data block in a different location. This way no single device is more critical than another anywhere in the infrastructure and critical data is always available. 
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>System high availability</td>
						<td>If a VirtualStor node encounters issues, the remaining nodes take over or balance the workload. This prevents a single point of failure</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">High Availability</p>
		                  <p class="lead-14">
			              Deploying VirtualStor™ to a data center entails installing at least three nodes. Each of the three nodes shoulder data delivery equally. If one or even two of the nodes are unavailable, IP takeover is used so one or more nodes seamlessly handle the service.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Plug and play architecture</td>
						<td>Storage nodes can be added without disruption to services or applications. Device configuration is not needed when a storage node is added.</td>
						<td>Data protection</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Plug and Play Architecture</p>
		                  <p class="lead-14">
			              VirtualStor™ automatically detects new storage nodes. Detected storage nodes immediately and seamlessly become part of the single massive storage pool VirtualStor™ manages. VirtualStor™ then begins balancing data equally across all the storage nodes in the storage pool. This means that data blocks are moved from nodes that were already a part of the storage pool to the new storage node.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Open management API</td>
						<td>Administrators can use VirtualStor APIs for further management in their infrastructure.</td>
						<td>Infrastructure agility</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Open Management API</p>
		                  <p class="lead-14">
			              VirtualStor™ provides administrators with an open management API. Administrators can use the management API to completely control each aspect of VirtualStor™ features.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->
				
				</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->

        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="performance">

        <!-- Table 3 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-weight-700 pi-text-base">Performance</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">Feature</th>
						<th>Description</th>
						<th>Benefit</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Scale-out storage</td>
						<td>Expand storage capacity and performance by adding more nodes</td>
						<td>Cost effective non-disruptive capacity and performance expansion</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Scale out architecture</p>
		                  <p class="lead-14">
			              Storage nodes can be added on the fly to VirtualStor™ with no disruption of service to applications or application workloads. As VirtualStor™ scales out, capacity is not the only benefit that data centers receive. Both throughput and IOPS also improve as VirtualStor™ scales out. This means storage capacity, data delivery, and raw processing power are all significantly improved.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Quality of Service</td>
						<td>Administrators can specify the performance ceiling for applications or workloads</td>
						<td>Resource management</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Quality of Service (QoS)</p>
		                  <p class="lead-14">
			              Administrators can use VirtualStor™ to provide Virtual Storage for their customers using policies. Policies specify the capacity and type of storage, the QoS, accessibility, and availability. QoS for Virtual Storage is composed of three components: IOPS, throughput, and latency. Both the IOPS and throughput ceilings can be configured, while latency is optimized automatically depending on the application or data delivery requests. While high performance flash-based SSD improves IOPS, scaling-out resources improves IOPS and throughput.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Flash-based SSD acceleration</td>
						<td>Accelerate IOPS using SSD caching technology</td>
						<td>High performance (IOPS) for applications and data delivery</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">SSD Acceleration</p>
		                  <p class="lead-14">
			              VirtualStor™ brings blazing performance to your data center in several ways. First by leveraging SSD, VirtualStor™ improves data workload and application performance. When an administrator enables SSD acceleration, VirtualStor™ accesses randomly stored data, processes the data, then VirtualStor™ writes the data sequentially to HDD. Combining SSD caching and sequential writing, VirtualStor™ can improve traditional HDD storage performance by at least 10X. Administrators can improve performance further by adding more SSD (SATA/SAS, PCIe) or by scaling VirtualStor™ out.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Performance tiering</td>
						<td>Specify different storage resources for applications or workloads to match their requirements</td>
						<td>Applications and workloads get the storage and compute resources to match their requirements</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Performance Tiering</p>
		                  <p class="lead-14">
			              VirtualStor™ supports specifying the type of storage used for applications and data. This means that applications and data that require high performance can be assigned flash-based SSD, while other applications and data can be assigned HDD, and data for archive could be assigned cloud-based storage (Amazon S3, OpenStack Swift).
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Data location awareness</td>
						<td>Data is moved closer to applications for improved performance</td>
						<td>Efficient resource utilization</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Data Location Awareness</p>
		                  <p class="lead-14">
			              VirtualStor™ breaks data down into smaller data blocks. Those blocks are stored sequentially for quick data delivery on a storage device that VirtualStor™ manages. As applications make requests for the data VirtualStor™ automatically moves the data as close as possible to the application. This provides improved data delivery performance.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>VAAI support</td>
						<td>Support VMware vSphere Storage APIs – Array Integration(VAAI) to utilize storage hardware to accomplish certain tasks</td>
						<td>Improve VMware I/O performance</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>Memory caching cluster</td>
						<td>Clustering unused memory as a cache for applications and workloads</td>
						<td>High performance (IOPS) for applications and data delivery and infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>Caching pool</td>
						<td>Administrators can create cache pools for slower storage pools to increase hot data accessing performance</td>
						<td>Improved application and workload performance</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="resilience">

        <!-- Table 4 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-weight-700 pi-text-base">System Resilience</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">Feature</th>
						<th>Description</th>
						<th>Benefit</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Load balancing</td>
						<td>Data requests and delivery are balanced equally across storage nodes</td>
						<td>Infrastructure performance</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Load Balancing</p>
		                  <p class="lead-14">
			              Requests for data are distributed across the nodes available to VirtualStor™. The more nodes that are available, the better the throughput performance. This means that as VirtualStor™ scales out, the throughput performance increases.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Self-repairing</td>
						<td>Missing or damaged data blocks are repaired by using RAID, data replication, or erasure coding technologies</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Self Repairing</p>
		                  <p class="lead-14">
			              There are a few ways that VirtualStor™ can repair the storage infrastructure. The way a repair occurs depends on which data protection features are enabled.
		                  </p>
		                  <p class="lead-14">
			              With data replication enabled, as VirtualStor™ breaks data up into blocks, a minimum of two copies for each data block is created and then distributed at different locations in the infrastructure. When VirtualStor™ detects a device is damaged or a data block is missing or corrupt, VirtualStor™ creates another duplicate of the block and saves the data block in a different location. This way no single device is more critical than another anywhere in the infrastructure and critical data is always available.
		                  </p>
		                  <p class="lead-14">
			              Erasure coding takes a different approach. VirtualStor™ breaks data down into blocks (a file is broken into blocks A, B, C, and D) and distributes the blocks across the storage infrastructure. When erasure coding is enabled, a parity file is created for the blocks to recreate the original file. If any of the blocks are corrupt or deleted, VirtualStor™ uses the parity file to recreate the deleted or corrupt data blocks. The software RAID uses an approach that is very similar to erasure coding.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>System high availability</td>
						<td>If a VirtualStor node encounters issues, the remaining nodes take over or balance the workload. This prevents a single point of failure.</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">High Availability</p>
		                  <p class="lead-14">
			              Deploying VirtualStor™ to a data center entails installing at least three nodes. Each of the three nodes shoulder data delivery equally. If one or even two of the nodes are unavailable, IP takeover is used so one or more nodes seamlessly handle the service.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Plug and play architecture</td>
						<td>Storage nodes can be added without disruption to services or applications. Device configuration is not needed when a storage node is added.</td>
						<td>Data protection</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Plug and Play Architecture</p>
		                  <p class="lead-14">
			              VirtualStor™ automatically detects new storage nodes. Detected storage nodes immediately and seamlessly become part of the single massive storage pool VirtualStor™ manages. VirtualStor™ then begins balancing data equally across all the storage nodes in the storage pool. This means that data blocks are moved from nodes that were already a part of the storage pool to the new storage node.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>IP takeover</td>
						<td>Seamless service take over from storage node to storage node when issues occur within the same cluster.</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">IP Takeover</p>
		                  <p class="lead-14">
			              Deploying VirtualStor™ to a data center entails installing at least three nodes. Each of the three nodes shoulder data delivery equally. If one or even two of the nodes are unavailable, IP takeover is used so one or more nodes seamlessly handle the service.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Decentralized architecture</td>
						<td>VirtualStor deploys as three or more nodes across an infrastructure. As VirtualStor deploys, storage becomes a single massive storage entity. If one of the nodes encounters issues the other nodes take over, ensuring service continuity.</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Decentralized Management Console</p>
		                  <p class="lead-14">
			              Management is kept simple because all of the storage types appear in a single management console provided by VirtualStor™. VirtualStor™ provides a single decentralized management portal for all storage types (SAN, NAS, CAS). The management portal is available as long as at least one of the VirtualStor™ appliances is working. The management portal provides a graphical dashboard for at-a-glance monitoring of storage resources and of the process management for storage devices, and VirtualStor™. Administrators can also set up alerts for proactive monitoring of their resources.
		                  </p>
		                  <p class="lead-14">
			              The VirtualStor™ management console delivers complete control over storage resources to administrators. Administrators can intuitively configure the storage type (NAS, SAN, CAS), capacity, QoS (IOPS, throughput), availability of data (for applications and storage), and data services (compression, deduplication, encryption) for each Virtual Storage area. VirtualStor™ also provides open restful management APIs for administrators to seemlessly integrate VirtualStor™ with their management framework for infrastructure or other business processes.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>SAN volume clone</td>
						<td>Copy-on-write clones for SAN volumes from an existing snapshot</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>OSD high availability in Active-Active mode</td>
						<td>If an object storage device (OSD) enounters issues, workload traffic is redirected or load balanced to OSD that are working properly</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>Multi-path support for external attached storage</td>
						<td>External attached SAN storage can be configured with multiple paths to prevent single path failures</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

        <!-- Tabs content item -->
		<div class="pi-tab-pane" id="protection">

        <!-- Table 5 -->
		<div class="pi-responsive-table-sm">
			<p class="lead-26 pi-weight-700 pi-text-base">Data Protection</p>
			<table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">
			
				<!-- Table head -->
				<thead>
				
					<!-- Table row -->
					<tr>
						<th style="width:24%">Feature</th>
						<th>Description</th>
						<th>Benefit</th>
					</tr>
					<!-- End table row -->
				
				</thead>
				<!-- End table head -->
				
				<!-- Table body -->
				<tbody>

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Real-time data replication</td>
						<td>Provide better data availability and performance by creating duplicate data (up to 10 copies)</td>
						<td>Configure data availability</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Data Replication</p>
		                  <p class="lead-14">
			              VirtualStor™ breaks data up into manageable blocks and then distribute the blocks of data across the storage available. With a minimum of two copies of a block created and distributed at different locations in the infrastructure. Administrators can configure VirtualStor™ to create up to 10 copies of each data block. Each additional copy of a data block increases the data protection and availability of the infrastructure. Replication can be configured for applications or Virtual Storage areas. Creating more copies of a data block also improves throughput performance, because VirtualStor™ intelligently monitors the blocks in use and will direct applications and workloads to unused copies of the data.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Remote data replication</td>
						<td>Replicate data to remote or branch locations</td>
						<td>Data consistency across the infrastructure</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Data consistency across the infrastructure</p>
		                  <p class="lead-14">
			              Administrators can asynchronously replicate data to remote locations. This is typically done for data protection or backups. For example, a head office may want data replication to branch offices or multiple branch offices may want data replicated to the head office.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Continuous data protection</td>
						<td>Up to the minute efficient backups and quick recovery, using storage snapshot technology</td>
						<td>Infrastructure resilience and availability</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Continuous Data Protection</p>
		                  <p class="lead-14">
			              VirtualStor™ continuously monitors the data stored across the storage devices that VirtualStor™ manages. Administrators can configure VirtualStor™ to create efficient up to the minute snapshots of the stored data. This gives administrators the option to backup and restore data at whim.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Data encryption</td>
						<td>Real-time data encryption/decryption technology using Intel AES-NI (when available) or by 256 bit software encryption</td>
						<td>Data security</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Data Encryption</p>
		                  <p class="lead-14">
			              VirtualStor™ can encrypt data stored on S3 storage using Intel’s AES-NI encryption technology (when available) or a 256-bit software encryption algorithm.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Erasure coding</td>
						<td>Provides network RAID-like protection technology across storage nodes</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Erasure Coding</p>
		                  <p class="lead-14">
			              VirtualStor™ breaks data down into blocks (a file is broken into blocks A, B, C, and D) and distributes the blocks across the storage infrastructure. When erasure coding is enabled, a parity file is created for the blocks to recreate the original file. If any of the blocks are corrupt or deleted, VirtualStor™ uses the parity file to recreate the deleted or corrupt data blocks.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Data balancing</td>
						<td>Balance data equally across storage devices</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Data Balancing</p>
		                  <p class="lead-14">
			              VirtualStor™ breaks data down into blocks (a file is broken into blocks A, B, C, and D) and distributes the blocks across the storage infrastructure. VirtualStor™ distributes the data blocks evenly across the storage infrastructure.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Self-repairing</td>
						<td>Missing or damaged data blocks are repaired by using RAID, data replication, or erasure coding technologies</td>
						<td>Infrastructure resilience</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Self Repairing</p>
		                  <p class="lead-14">
			              There are a few ways that VirtualStor™ can repair the storage infrastructure. The way a repair occurs depends on which data protection features are enabled.
		                  </p>
		                  <p class="lead-14">
			              With data replication enabled, as VirtualStor™ breaks data up into blocks, a minimum of two copies for each data block is created and then distributed at different locations in the infrastructure. When VirtualStor™ detects a device is damaged or a data block is missing or corrupt, VirtualStor™ creates another duplicate of the block and saves the data block in a different location. This way no single device is more critical than another anywhere in the infrastructure and critical data is always available.
		                  </p>
		                  <p class="lead-14">
			              Erasure coding takes a different approach. VirtualStor™ breaks data down into blocks (a file is broken into blocks A, B, C, and D) and distributes the blocks across the storage infrastructure. When erasure coding is enabled, a parity file is created for the blocks to recreate the original file. If any of the blocks are corrupt or deleted, VirtualStor™ uses the parity file to recreate the deleted or corrupt data blocks. The software RAID uses an approach that is very similar to erasure coding.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Cloud backup and restore</td>
						<td>Backup data to Amazon S3 storage (or storage that mimics Amazon S3)</td>
						<td>Data protection</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Cloud Backup and Restore</p>
		                  <p class="lead-14">
			              Cloud storage offers the most cost effective option for businesses. VirtualStor™ gives administrators the ability to back up their data to and restore their data from the cloud (Amazon S3 or OpenStack Swift).
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick link">
						<td>Data migration</td>
						<td>As applications request data, data stored on legacy storage is automatically moved to VirtualStor.</td>
						<td>Efficient resource utilization</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3">
					      <p class="lead-26 pi-weight-700 pi-text-base pi-padding-top-10">Data Migration</p>
		                  <p class="lead-14">
			              VirtualStor™ creates a single seamless agile pool of resources from all storage resources VirtualStor™ manages. As applications request data on older storage devices VirtualStor™ automatically moves the data to newer storage devices. This automatic data migration means that older or damaged storage devices can be replaced on-the-fly without any impact to the level of service for your customers or the business continuity of your enterprise.
		                  </p>
						</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr class="rowclick">
						<td>SAN (FC) access control</td>
						<td>Administrators can define user access to SAN (FC) storage</td>
						<td>Data protection</td>
					</tr>
					<!-- End table row -->

					<!-- Table row -->
					<tr hidden>
						<td colspan="3" class="tdempty"></td>
					</tr>
					<!-- End table row -->

					</tbody>
				<!-- End table body -->
				
			</table>
		</div>
		<!-- End table -->
        </div>

	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>