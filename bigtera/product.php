<?php include_once('config.php'); ?>
<?php
$title = "Bigtera - Products";
require_once(ROOT ."inc/header.inc");
?>

<div id="page">

<!-- Title bar -->
<div class="pi-section-w pi-section-base pi-section-base-gradient">
	<div class="pi-texture" style="background: url(<?php echo $url; ?>img/hexagon.png) repeat;"></div>
	<div class="pi-section" style="padding: 30px 40px 26px;">
	
		<div class="pi-row">
			<div class="pi-col-sm-4 pi-center-text-xs">
				<h1 class="h2 pi-weight-300 pi-margin-bottom-5">Products</h1>
			</div>
		</div>
		
	</div>
</div>
<!-- End title bar -->

<!-- Breadcrumbs -->
<div class="pi-section-w pi-border-bottom pi-section-grey">
	<div class="pi-section pi-titlebar pi-breadcrumb-only">
		<div class="pi-breadcrumb pi-center-text-xs">
			<ul>
				<li><a href="<?php echo $url; ?>">Home</a></li>
				<li>Products</li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->

<!-- - - - - - - - - - SECTION - - - - - - - - - -->

<div class="pi-section-w pi-section-white pi-slider-enabled"><a id="sds"></a>
	<div class="pi-section pi-padding-bottom-40">
	    <div class="pi-row pi-padding-top-30">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:50px">
	      	<img src="<?php echo $url; ?>img/product/1U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ SDS Controller</p>
		<p class="lead-14">
			Consolidates, simplifies, and optimizes storage infrastructures
		</p>
		<p class="lead-14">
			VirtualStor™ SDS Controller significantly increases the ROI for storage infrastructures while at the same time revitalizing and enhancing the infrastructure. SDS Controller consolidates and virtually aggregates SAN, NAS, and DAS storage products in to a single massive, easily managed, flexible, storage entity. From this entity, administrators can create Virtual Storage pools (SAN, NAS, object) and apply a number of data services, all while defining storage capacity, performance (IOPS and throughput), accessibility, and availability.
		</p>
		<p class="lead-14">
			<a href="http://www.bigtera.com/en/docs/Bigtera%20VirtualStor%20Controller-Datasheet_en_z.pdf">Download SDS Controller Datasheet</a>
		</p>
		  </div>
	    </div>
	    <a id="scaler"></a>
		<div class="pi-row pi-padding-top-80">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:20px">
	      	<img src="<?php echo $url; ?>img/product/2U4U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ Scaler</p>
		<p class="lead-14">
			High-performance unified scale-out storage
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			VirtualStor™ Scaler provides customers with a cost effective x86 scale-out storage solution that allows them to pay as they grow. Scaler’s scale-out architecture provides the flexibility to specify the storage type (NAS, SAN, object storage), performance (IOPS and throughput), and efficiency all while delivering resilient and secure capacity.
		</p>
		<p class="lead-14">
			<a href="http://www.bigtera.com/en/docs/Bigtera%20VirtualStor%20Scaler-Datasheet_en_z.pdf">Download Scaler Datasheet</a>
		</p>
		  </div>
	    </div>
	    <a id="converger"></a>
        <div class="pi-row pi-padding-top-50">
		  <div class="pi-col-sm-3" style="text-align:right; padding-top:30px">
	      	<img src="<?php echo $url; ?>img/product/2U.png" alt="">
	      </div>
		  <div class="pi-col-sm-9">
		<p class="lead-26 pi-weight-700 pi-text-base pi-margin-bottom-5">VirtualStor™ Converger</p>
		<p class="lead-14">
			Cross-platform unified converged storage
		</p>
		<p class="lead-14 pi-padding-bottom-30">
			VirtualStor™ Converger provides customers with a hyper-converged, flexible, scale out storage solution on x86 server. Converger deploys to any hypervisor (KVM, VMware, Hyper-V) blending high performance with simplified infrastructure management.
		</p>
		<p class="lead-14">
			<a href="http://www.bigtera.com/en/docs/Bigtera%20VirtualStor%20Converger-Datasheet_en_z.pdf">Download Converger Daftasheet</a>
		</p>		
		  </div>
	    </div>
	</div>
</div>

<!-- - - - - - - - - - END SECTION - - - - - - - - - -->

</div>

<?php require_once(ROOT ."inc/footer.inc"); ?>

</div>

<?php require_once(ROOT ."inc/common.inc"); ?>

</body>
</html>
